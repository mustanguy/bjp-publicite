CREATE TABLE documents_category (id INT AUTO_INCREMENT NOT NULL, category VARCHAR(150) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB;

INSERT INTO `documents_category` (`id`, `category`) VALUES ('1', 'SUPPORTS DE VENTE WEB'), ('2', 'SUPPORTS DE VENTE PRINT'), ('3', 'TARIFS'), ('4', 'OPERATIONS SPECIALES'), ('5', 'FICHES TECHNIQUES'), ('6', 'DIVERS'), ('7', 'ACTUALITE DIGITALE');

UPDATE `documents` SET `category` = 1 WHERE `category` = 'SUPPORTS DE VENTE WEB';
UPDATE `documents` SET `category` = 2 WHERE `category` = 'SUPPORTS DE VENTE PRINT';
UPDATE `documents` SET `category` = 3 WHERE `category` = 'TARIFS';
UPDATE `documents` SET `category` = 4 WHERE `category` = 'OPERATIONS SPECIALES';
UPDATE `documents` SET `category` = 5 WHERE `category` = 'FICHES TECHNIQUES';
UPDATE `documents` SET `category` = 6 WHERE `category` = 'DIVERS';
UPDATE `documents` SET `category` = 7 WHERE `category` = 'ACTUALITE DIGITALE';

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * FormDemandeProspe
 *
 * @ORM\Table(name="form_demande_prospe")
 * @ORM\Entity
 */
class FormDemandeProspe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="media", type="string", length=100, nullable=false)
     */
    private $media;

    /**
     * @var string
     *
     * @ORM\Column(name="departementcommercial", type="string", length=100, nullable=false)
     */
    private $departementCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nomcommercial", type="string", length=100, nullable=false)
     */
    private $nomcommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="prenomcommercial", type="string", length=100, nullable=false)
     */
    private $prenomcommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="emailcommercial", type="string", length=100, nullable=false)
     */
    private $emailcommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="telephonecommercial", type="string", length=15, nullable=false)
     */
    private $telephonecommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nomclient", type="string", length=100, nullable=false)
     */
    private $nomclient;

    /**
     * @var string
     *
     * @ORM\Column(name="sitewebclient", type="string", length=100, nullable=false)
     */
    private $sitewebclient;

    /**
     * @var string
     *
     * @ORM\Column(name="listeformatsbase", type="string", length=512, nullable=false)
     */
    private $listeformatsbase;

    /**
     * @var string
     *
     * @ORM\Column(name="listezonediffusionbase", type="string", length=512, nullable=false)
     */
    private $listezonediffusionbase;

    /**
     * @var string
     *
     * @ORM\Column(name="rubrique", type="string", length=100, nullable=true)
     */
    private $rubrique;

    /**
     * @var string
     *
     * @ORM\Column(name="listetypebase", type="string", length=512, nullable=false)
     */
    private $listetypebase;

    /**
     * @var string
     *
     * @ORM\Column(name="briefing", type="string", length=512, nullable=false)
     */
    private $briefing;

    /**
     * @var string
     *
     * @ORM\Column(name="file1", type="text", nullable=true)
     */
    private $file1;

    /**
     * @var string
     *
     * @ORM\Column(name="file2", type="text", nullable=true)
     */
    private $file2;

    /**
     * @var string
     *
     * @ORM\Column(name="file3", type="text", nullable=true)
     */
    private $file3;

    /**
     * @var string
     *
     * @ORM\Column(name="file4", type="text", nullable=true)
     */
    private $file4;

    /**
     * FormDemandeProspe constructor.
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param string $media
     */
    public function setMedia($media)
    {
        $this->media = $media;
    }

    /**
     * @return string
     */
    public function getDepartementCommercial()
    {
        return $this->departementCommercial;
    }

    /**
     * @param string $departementCommercial
     */
    public function setDepartementCommercial($departementCommercial)
    {
        $this->departementCommercial = $departementCommercial;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return FormDemandeProspe
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set nomcommercial
     *
     * @param string $nomcommercial
     *
     * @return FormDemandeProspe
     */
    public function setNomcommercial($nomcommercial)
    {
        $this->nomcommercial = $nomcommercial;

        return $this;
    }

    /**
     * Get nomcommercial
     *
     * @return string
     */
    public function getNomcommercial()
    {
        return $this->nomcommercial;
    }

    /**
     * Set prenomcommercial
     *
     * @param string $prenomcommercial
     *
     * @return FormDemandeProspe
     */
    public function setPrenomcommercial($prenomcommercial)
    {
        $this->prenomcommercial = $prenomcommercial;

        return $this;
    }

    /**
     * Get prenomcommercial
     *
     * @return string
     */
    public function getPrenomcommercial()
    {
        return $this->prenomcommercial;
    }

    /**
     * Set emailcommercial
     *
     * @param string $emailcommercial
     *
     * @return FormDemandeProspe
     */
    public function setEmailcommercial($emailcommercial)
    {
        $this->emailcommercial = $emailcommercial;

        return $this;
    }

    /**
     * Get emailcommercial
     *
     * @return string
     */
    public function getEmailcommercial()
    {
        return $this->emailcommercial;
    }

    /**
     * Set telephonecommercial
     *
     * @param string $telephonecommercial
     *
     * @return FormDemandeProspe
     */
    public function setTelephonecommercial($telephonecommercial)
    {
        $this->telephonecommercial = $telephonecommercial;

        return $this;
    }

    /**
     * Get telephonecommercial
     *
     * @return string
     */
    public function getTelephonecommercial()
    {
        return $this->telephonecommercial;
    }

    /**
     * Set nomclient
     *
     * @param string $nomclient
     *
     * @return FormDemandeProspe
     */
    public function setNomclient($nomclient)
    {
        $this->nomclient = $nomclient;

        return $this;
    }

    /**
     * Get nomclient
     *
     * @return string
     */
    public function getNomclient()
    {
        return $this->nomclient;
    }

    /**
     * Set sitewebclient
     *
     * @param string $sitewebclient
     *
     * @return FormDemandeProspe
     */
    public function setSitewebclient($sitewebclient)
    {
        $this->sitewebclient = $sitewebclient;

        return $this;
    }

    /**
     * Get sitewebclient
     *
     * @return string
     */
    public function getSitewebclient()
    {
        return $this->sitewebclient;
    }

    /**
     * Set listeformatsbase
     *
     * @param string $listeformatsbase
     *
     * @return FormDemandeProspe
     */
    public function setListeformatsbase($listeformatsbase)
    {
        $this->listeformatsbase = $listeformatsbase;

        return $this;
    }

    /**
     * Get listeformatsbase
     *
     * @return string
     */
    public function getListeformatsbase()
    {
        return $this->listeformatsbase;
    }

    /**
     * Set listezonediffusionbase
     *
     * @param string $listezonediffusionbase
     *
     * @return FormDemandeProspe
     */
    public function setListezonediffusionbase($listezonediffusionbase)
    {
        $this->listezonediffusionbase = $listezonediffusionbase;

        return $this;
    }

    /**
     * Get listezonediffusionbase
     *
     * @return string
     */
    public function getListezonediffusionbase()
    {
        return $this->listezonediffusionbase;
    }

    /**
     * Set rubrique
     *
     * @param string $rubrique
     *
     * @return FormDemandeProspe
     */
    public function setRubrique($rubrique)
    {
        $this->rubrique = $rubrique;

        return $this;
    }

    /**
     * Get rubrique
     *
     * @return string
     */
    public function getRubrique()
    {
        return $this->rubrique;
    }

    /**
     * Set listetypebase
     *
     * @param string $listetypebase
     *
     * @return FormDemandeProspe
     */
    public function setListetypebase($listetypebase)
    {
        $this->listetypebase = implode(' - ',$listetypebase);

        return $this;
    }

    /**
     * Get listetypebase
     *
     * @return string
     */
    public function getListetypebase()
    {
        $listetypebaseTab = explode(' - ', $this->listetypebase);

        return $listetypebaseTab;
    }

    /**
     * Set briefing
     *
     * @param string $briefing
     *
     * @return FormDemandeProspe
     */
    public function setBriefing($briefing)
    {
        $this->briefing = $briefing;

        return $this;
    }

    /**
     * Get briefing
     *
     * @return string
     */
    public function getBriefing()
    {
        return $this->briefing;
    }

    /**
     * @return string
     */
    public function getFile1()
    {
        return $this->file1;
    }

    /**
     * @param string $file1
     */
    public function setFile1($file1)
    {
        $this->file1 = $file1;
    }

    /**
     * @return string
     */
    public function getFile2()
    {
        return $this->file2;
    }

    /**
     * @param string $file2
     */
    public function setFile2($file2)
    {
        $this->file2 = $file2;
    }

    /**
     * @return string
     */
    public function getFile3()
    {
        return $this->file3;
    }

    /**
     * @param string $file3
     */
    public function setFile3($file3)
    {
        $this->file3 = $file3;
    }

    /**
     * @return string
     */
    public function getFile4()
    {
        return $this->file4;
    }

    /**
     * @param string $file4
     */
    public function setFile4($file4)
    {
        $this->file4 = $file4;
    }

}

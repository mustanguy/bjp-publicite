<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Events
 *
 * @ORM\Table(name="events")
 * @ORM\Entity
 */
class Events
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_event", type="string", length=255, nullable=false)
     */
    private $nomEvent;

    /**
     * @var string
     *
     * @ORM\Column(name="orga", type="string", length=5, nullable=false)
     */
    private $orga;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_cdp", type="string", length=150, nullable=false)
     */
    private $nomCdp;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_cdp", type="string", length=150, nullable=false)
     */
    private $telCdp;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_cdp", type="string", length=150, nullable=false)
     */
    private $mailCdp;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_referent", type="string", length=150, nullable=false)
     */
    private $nomReferent;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_referent", type="string", length=150, nullable=false)
     */
    private $telReferent;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_referent", type="string", length=150, nullable=false)
     */
    private $mailReferent;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_assistant", type="string", length=150, nullable=false)
     */
    private $nomAssistant;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_assistant", type="string", length=150, nullable=false)
     */
    private $telAssistant;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_assistant", type="string", length=150, nullable=false)
     */
    private $mailAssistant;

    /**
     * @var string
     *
     * @ORM\Column(name="crea_reprise", type="string", length=10, nullable=false)
     */
    private $creaReprise;

    /**
     * @var string
     *
     * @ORM\Column(name="wetransfert1", type="string", length=255, nullable=false)
     */
    private $wetransfert1;

    /**
     * @var string
     *
     * @ORM\Column(name="wetransfert2", type="string", length=255, nullable=false)
     */
    private $wetransfert2;

    /**
     * @var string
     *
     * @ORM\Column(name="wetransfert3", type="string", length=255, nullable=false)
     */
    private $wetransfert3;

    /**
     * @var string
     *
     * @ORM\Column(name="brief", type="text", length=65535, nullable=false)
     */
    private $brief;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_event", type="datetime", nullable=false)
     */
    private $dateEvent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_rendu", type="datetime", nullable=false)
     */
    private $dateRendu;

    /**
     * @var string
     *
     * @ORM\Column(name="kit", type="string", length=100, nullable=false)
     */
    private $kit;

    /**
     * @var string
     *
     * @ORM\Column(name="kit_autre", type="string", length=100, nullable=true)
     */
    private $kitAutre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_livraison", type="datetime", nullable=false)
     */
    private $dateLivraison;

    /**
     * @var integer
     *
     * @ORM\Column(name="quotidien_oi1", type="integer", nullable=false)
     */
    private $quotidienOi1;

    /**
     * @var integer
     *
     * @ORM\Column(name="quotidien_oi2", type="integer", nullable=false)
     */
    private $quotidienOi2;

    /**
     * @var integer
     *
     * @ORM\Column(name="quotidien_oi3", type="integer", nullable=false)
     */
    private $quotidienOi3;

    /**
     * @var integer
     *
     * @ORM\Column(name="cnews_oi1", type="integer", nullable=false)
     */
    private $cnewsOi1;

    /**
     * @var integer
     *
     * @ORM\Column(name="cnews_oi2", type="integer", nullable=false)
     */
    private $cnewsOi2;

    /**
     * @var integer
     *
     * @ORM\Column(name="cnews_oi3", type="integer", nullable=false)
     */
    private $cnewsOi3;

    /**
     * @var integer
     *
     * @ORM\Column(name="tv_oi1", type="integer", nullable=false)
     */
    private $tvOi1;

    /**
     * @var integer
     *
     * @ORM\Column(name="tv_oi2", type="integer", nullable=false)
     */
    private $tvOi2;

    /**
     * @var integer
     *
     * @ORM\Column(name="tv_oi3", type="integer", nullable=false)
     */
    private $tvOi3;

    /**
     * @var integer
     *
     * @ORM\Column(name="autre_oi1", type="integer", nullable=false)
     */
    private $autreOi1;

    /**
     * @var integer
     *
     * @ORM\Column(name="autre_oi2", type="integer", nullable=false)
     */
    private $autreOi2;

    /**
     * @var integer
     *
     * @ORM\Column(name="autre_oi3", type="integer", nullable=false)
     */
    private $autreOi3;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Events
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set nomEvent
     *
     * @param string $nomEvent
     *
     * @return Events
     */
    public function setNomEvent($nomEvent)
    {
        $this->nomEvent = $nomEvent;

        return $this;
    }

    /**
     * Get nomEvent
     *
     * @return string
     */
    public function getNomEvent()
    {
        return $this->nomEvent;
    }

    /**
     * Set orga
     *
     * @param string $orga
     *
     * @return Events
     */
    public function setOrga($orga)
    {
        $this->orga = $orga;

        return $this;
    }

    /**
     * Get orga
     *
     * @return string
     */
    public function getOrga()
    {
        return $this->orga;
    }

    /**
     * Set nomCdp
     *
     * @param string $nomCdp
     *
     * @return Events
     */
    public function setNomCdp($nomCdp)
    {
        $this->nomCdp = $nomCdp;

        return $this;
    }

    /**
     * Get nomCdp
     *
     * @return string
     */
    public function getNomCdp()
    {
        return $this->nomCdp;
    }

    /**
     * Set telCdp
     *
     * @param string $telCdp
     *
     * @return Events
     */
    public function setTelCdp($telCdp)
    {
        $this->telCdp = $telCdp;

        return $this;
    }

    /**
     * Get telCdp
     *
     * @return string
     */
    public function getTelCdp()
    {
        return $this->telCdp;
    }

    /**
     * Set mailCdp
     *
     * @param string $mailCdp
     *
     * @return Events
     */
    public function setMailCdp($mailCdp)
    {
        $this->mailCdp = $mailCdp;

        return $this;
    }

    /**
     * Get mailCdp
     *
     * @return string
     */
    public function getMailCdp()
    {
        return $this->mailCdp;
    }

    /**
     * Set nomReferent
     *
     * @param string $nomReferent
     *
     * @return Events
     */
    public function setNomReferent($nomReferent)
    {
        $this->nomReferent = $nomReferent;

        return $this;
    }

    /**
     * Get nomReferent
     *
     * @return string
     */
    public function getNomReferent()
    {
        return $this->nomReferent;
    }

    /**
     * Set telReferent
     *
     * @param string $telReferent
     *
     * @return Events
     */
    public function setTelReferent($telReferent)
    {
        $this->telReferent = $telReferent;

        return $this;
    }

    /**
     * Get telReferent
     *
     * @return string
     */
    public function getTelReferent()
    {
        return $this->telReferent;
    }

    /**
     * Set mailReferent
     *
     * @param string $mailReferent
     *
     * @return Events
     */
    public function setMailReferent($mailReferent)
    {
        $this->mailReferent = $mailReferent;

        return $this;
    }

    /**
     * Get mailReferent
     *
     * @return string
     */
    public function getMailReferent()
    {
        return $this->mailReferent;
    }

    /**
     * Set nomAssistant
     *
     * @param string $nomAssistant
     *
     * @return Events
     */
    public function setNomAssistant($nomAssistant)
    {
        $this->nomAssistant = $nomAssistant;

        return $this;
    }

    /**
     * Get nomAssistant
     *
     * @return string
     */
    public function getNomAssistant()
    {
        return $this->nomAssistant;
    }

    /**
     * Set telAssistant
     *
     * @param string $telAssistant
     *
     * @return Events
     */
    public function setTelAssistant($telAssistant)
    {
        $this->telAssistant = $telAssistant;

        return $this;
    }

    /**
     * Get telAssistant
     *
     * @return string
     */
    public function getTelAssistant()
    {
        return $this->telAssistant;
    }

    /**
     * Set mailAssistant
     *
     * @param string $mailAssistant
     *
     * @return Events
     */
    public function setMailAssistant($mailAssistant)
    {
        $this->mailAssistant = $mailAssistant;

        return $this;
    }

    /**
     * Get mailAssistant
     *
     * @return string
     */
    public function getMailAssistant()
    {
        return $this->mailAssistant;
    }

    /**
     * Set creaReprise
     *
     * @param string $creaReprise
     *
     * @return Events
     */
    public function setCreaReprise($creaReprise)
    {
        $this->creaReprise = $creaReprise;

        return $this;
    }

    /**
     * Get creaReprise
     *
     * @return string
     */
    public function getCreaReprise()
    {
        return $this->creaReprise;
    }

    /**
     * Set wetransfert1
     *
     * @param string $wetransfert1
     *
     * @return Events
     */
    public function setWetransfert1($wetransfert1)
    {
        $this->wetransfert1 = $wetransfert1;

        return $this;
    }

    /**
     * Get wetransfert1
     *
     * @return string
     */
    public function getWetransfert1()
    {
        return $this->wetransfert1;
    }

    /**
     * Set wetransfert2
     *
     * @param string $wetransfert2
     *
     * @return Events
     */
    public function setWetransfert2($wetransfert2)
    {
        $this->wetransfert2 = $wetransfert2;

        return $this;
    }

    /**
     * Get wetransfert2
     *
     * @return string
     */
    public function getWetransfert2()
    {
        return $this->wetransfert2;
    }

    /**
     * Set wetransfert3
     *
     * @param string $wetransfert3
     *
     * @return Events
     */
    public function setWetransfert3($wetransfert3)
    {
        $this->wetransfert3 = $wetransfert3;

        return $this;
    }

    /**
     * Get wetransfert3
     *
     * @return string
     */
    public function getWetransfert3()
    {
        return $this->wetransfert3;
    }

    /**
     * Set brief
     *
     * @param string $brief
     *
     * @return Events
     */
    public function setBrief($brief)
    {
        $this->brief = $brief;

        return $this;
    }

    /**
     * Get brief
     *
     * @return string
     */
    public function getBrief()
    {
        return $this->brief;
    }

    /**
     * Set dateEvent
     *
     * @param \DateTime $dateEvent
     *
     * @return Events
     */
    public function setDateEvent($dateEvent)
    {
        $this->dateEvent = $dateEvent;

        return $this;
    }

    /**
     * Get dateEvent
     *
     * @return \DateTime
     */
    public function getDateEvent()
    {
        return $this->dateEvent;
    }

    /**
     * Set dateRendu
     *
     * @param \DateTime $dateRendu
     *
     * @return Events
     */
    public function setDateRendu($dateRendu)
    {
        $this->dateRendu = $dateRendu;

        return $this;
    }

    /**
     * Get dateRendu
     *
     * @return \DateTime
     */
    public function getDateRendu()
    {
        return $this->dateRendu;
    }

    /**
     * Set kit
     *
     * @param string $kit
     *
     * @return Events
     */
    public function setKit($kit)
    {
        $this->kit = $kit;

        return $this;
    }

    /**
     * Get kit
     *
     * @return string
     */
    public function getKit()
    {
        return $this->kit;
    }

    /**
     * Set kitAutre
     *
     * @param string $kitAutre
     *
     * @return Events
     */
    public function setKitAutre($kitAutre)
    {
        $this->kitAutre = $kitAutre;

        return $this;
    }

    /**
     * Get kitAutre
     *
     * @return string
     */
    public function getKitAutre()
    {
        return $this->kitAutre;
    }

    /**
     * Set dateLivraison
     *
     * @param \DateTime $dateLivraison
     *
     * @return Events
     */
    public function setDateLivraison($dateLivraison)
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    /**
     * Get dateLivraison
     *
     * @return \DateTime
     */
    public function getDateLivraison()
    {
        return $this->dateLivraison;
    }

    /**
     * Set quotidienOi1
     *
     * @param integer $quotidienOi1
     *
     * @return Events
     */
    public function setQuotidienOi1($quotidienOi1)
    {
        $this->quotidienOi1 = $quotidienOi1;

        return $this;
    }

    /**
     * Get quotidienOi1
     *
     * @return integer
     */
    public function getQuotidienOi1()
    {
        return $this->quotidienOi1;
    }

    /**
     * Set quotidienOi2
     *
     * @param integer $quotidienOi2
     *
     * @return Events
     */
    public function setQuotidienOi2($quotidienOi2)
    {
        $this->quotidienOi2 = $quotidienOi2;

        return $this;
    }

    /**
     * Get quotidienOi2
     *
     * @return integer
     */
    public function getQuotidienOi2()
    {
        return $this->quotidienOi2;
    }

    /**
     * Set quotidienOi3
     *
     * @param integer $quotidienOi3
     *
     * @return Events
     */
    public function setQuotidienOi3($quotidienOi3)
    {
        $this->quotidienOi3 = $quotidienOi3;

        return $this;
    }

    /**
     * Get quotidienOi3
     *
     * @return integer
     */
    public function getQuotidienOi3()
    {
        return $this->quotidienOi3;
    }

    /**
     * Set cnewsOi1
     *
     * @param integer $cnewsOi1
     *
     * @return Events
     */
    public function setCnewsOi1($cnewsOi1)
    {
        $this->cnewsOi1 = $cnewsOi1;

        return $this;
    }

    /**
     * Get cnewsOi1
     *
     * @return integer
     */
    public function getCnewsOi1()
    {
        return $this->cnewsOi1;
    }

    /**
     * Set cnewsOi2
     *
     * @param integer $cnewsOi2
     *
     * @return Events
     */
    public function setCnewsOi2($cnewsOi2)
    {
        $this->cnewsOi2 = $cnewsOi2;

        return $this;
    }

    /**
     * Get cnewsOi2
     *
     * @return integer
     */
    public function getCnewsOi2()
    {
        return $this->cnewsOi2;
    }

    /**
     * Set cnewsOi3
     *
     * @param integer $cnewsOi3
     *
     * @return Events
     */
    public function setCnewsOi3($cnewsOi3)
    {
        $this->cnewsOi3 = $cnewsOi3;

        return $this;
    }

    /**
     * Get cnewsOi3
     *
     * @return integer
     */
    public function getCnewsOi3()
    {
        return $this->cnewsOi3;
    }

    /**
     * Set tvOi1
     *
     * @param integer $tvOi1
     *
     * @return Events
     */
    public function setTvOi1($tvOi1)
    {
        $this->tvOi1 = $tvOi1;

        return $this;
    }

    /**
     * Get tvOi1
     *
     * @return integer
     */
    public function getTvOi1()
    {
        return $this->tvOi1;
    }

    /**
     * Set tvOi2
     *
     * @param integer $tvOi2
     *
     * @return Events
     */
    public function setTvOi2($tvOi2)
    {
        $this->tvOi2 = $tvOi2;

        return $this;
    }

    /**
     * Get tvOi2
     *
     * @return integer
     */
    public function getTvOi2()
    {
        return $this->tvOi2;
    }

    /**
     * Set tvOi3
     *
     * @param integer $tvOi3
     *
     * @return Events
     */
    public function setTvOi3($tvOi3)
    {
        $this->tvOi3 = $tvOi3;

        return $this;
    }

    /**
     * Get tvOi3
     *
     * @return integer
     */
    public function getTvOi3()
    {
        return $this->tvOi3;
    }

    /**
     * Set autreOi1
     *
     * @param integer $autreOi1
     *
     * @return Events
     */
    public function setAutreOi1($autreOi1)
    {
        $this->autreOi1 = $autreOi1;

        return $this;
    }

    /**
     * Get autreOi1
     *
     * @return integer
     */
    public function getAutreOi1()
    {
        return $this->autreOi1;
    }

    /**
     * Set autreOi2
     *
     * @param integer $autreOi2
     *
     * @return Events
     */
    public function setAutreOi2($autreOi2)
    {
        $this->autreOi2 = $autreOi2;

        return $this;
    }

    /**
     * Get autreOi2
     *
     * @return integer
     */
    public function getAutreOi2()
    {
        return $this->autreOi2;
    }

    /**
     * Set autreOi3
     *
     * @param integer $autreOi3
     *
     * @return Events
     */
    public function setAutreOi3($autreOi3)
    {
        $this->autreOi3 = $autreOi3;

        return $this;
    }

    /**
     * Get autreOi3
     *
     * @return integer
     */
    public function getAutreOi3()
    {
        return $this->autreOi3;
    }
}

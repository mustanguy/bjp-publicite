<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VidsForm
 *
 * @ORM\Table(name="vids_form")
 * @ORM\Entity
 */
class VidsForm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="societe", type="string", length=150, nullable=true)
     */
    private $societe;

    /**
     * @var string
     *
     * @ORM\Column(name="siteweb_client", type="string", length=255, nullable=true)
     */
    private $sitewebClient;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_client", type="string", length=200, nullable=true)
     */
    private $nomClient;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_client", type="string", length=255, nullable=true)
     */
    private $mailClient;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_client", type="string", length=255, nullable=false)
     */
    private $adresseClient;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_client", type="string", length=15, nullable=true)
     */
    private $telClient;

    /**
     * @var string
     *
     * @ORM\Column(name="page_client", type="string", length=255, nullable=true)
     */
    private $pageClient;

    /**
     * @var string
     *
     * @ORM\Column(name="activite_client", type="string", length=255, nullable=true)
     */
    private $activiteClient;

    /**
     * @var string
     *
     * @ORM\Column(name="opttech", type="string", length=40, nullable=true)
     */
    private $opttech;

    /**
     * @var string
     *
     * @ORM\Column(name="commercial", type="string", length=255, nullable=false)
     */
    private $commercial;

    /**
     * @var string
     *
     * @ORM\Column(name="datelivraison", type="datetime", length=50, nullable=true)
     */
    private $datelivraison;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", length=65535, nullable=true)
     */
    private $observations;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_video", type="string", length=255, nullable=true)
     */
    private $titreVideo;

    /**
     * @var string
     *
     * @ORM\Column(name="num_ordre", type="string", length=50, nullable=true)
     */
    private $numOrdre;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_twitter", type="string", length=255, nullable=true)
     */
    private $reseauTwitter;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_facebook", type="string", length=255, nullable=true)
     */
    private $reseauFacebook;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_linkedin", type="string", length=255, nullable=true)
     */
    private $reseauLinkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_youtube", type="string", length=255, nullable=true)
     */
    private $reseauYoutube;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_insta", type="string", length=255, nullable=true)
     */
    private $reseauInsta;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_pinterest", type="string", length=255, nullable=true)
     */
    private $reseauPinterest;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_viadeo", type="string", length=255, nullable=true)
     */
    private $reseauViadeo;

    /**
     * @var string
     *
     * @ORM\Column(name="reseau_google", type="string", length=255, nullable=true)
     */
    private $reseauGoogle;

    /**
     * @var string
     *
     * @ORM\Column(name="cible", type="string", length=255, nullable=true)
     */
    private $cible;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255, nullable=true)
     */
    private $lieu;


    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return VidsForm
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set societe
     *
     * @param string $societe
     *
     * @return VidsForm
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set sitewebClient
     *
     * @param string $sitewebClient
     *
     * @return VidsForm
     */
    public function setSitewebClient($sitewebClient)
    {
        $this->sitewebClient = $sitewebClient;

        return $this;
    }

    /**
     * Get sitewebClient
     *
     * @return string
     */
    public function getSitewebClient()
    {
        return $this->sitewebClient;
    }

    /**
     * Set nomClient
     *
     * @param string $nomClient
     *
     * @return VidsForm
     */
    public function setNomClient($nomClient)
    {
        $this->nomClient = $nomClient;

        return $this;
    }

    /**
     * Get nomClient
     *
     * @return string
     */
    public function getNomClient()
    {
        return $this->nomClient;
    }

    /**
     * Set mailClient
     *
     * @param string $mailClient
     *
     * @return VidsForm
     */
    public function setMailClient($mailClient)
    {
        $this->mailClient = $mailClient;

        return $this;
    }

    /**
     * Get mailClient
     *
     * @return string
     */
    public function getMailClient()
    {
        return $this->mailClient;
    }

    /**
     * Set adresseClient
     *
     * @param string $adresseClient
     *
     * @return VidsForm
     */
    public function setAdresseClient($adresseClient)
    {
        $this->adresseClient = $adresseClient;

        return $this;
    }

    /**
     * Get adresseClient
     *
     * @return string
     */
    public function getAdresseClient()
    {
        return $this->adresseClient;
    }

    /**
     * Set telClient
     *
     * @param string $telClient
     *
     * @return VidsForm
     */
    public function setTelClient($telClient)
    {
        $this->telClient = $telClient;

        return $this;
    }

    /**
     * Get telClient
     *
     * @return string
     */
    public function getTelClient()
    {
        return $this->telClient;
    }

    /**
     * Set pageClient
     *
     * @param string $pageClient
     *
     * @return VidsForm
     */
    public function setPageClient($pageClient)
    {
        $this->pageClient = $pageClient;

        return $this;
    }

    /**
     * Get pageClient
     *
     * @return string
     */
    public function getPageClient()
    {
        return $this->pageClient;
    }

    /**
     * Set activiteClient
     *
     * @param string $activiteClient
     *
     * @return VidsForm
     */
    public function setActiviteClient($activiteClient)
    {
        $this->activiteClient = $activiteClient;

        return $this;
    }

    /**
     * Get activiteClient
     *
     * @return string
     */
    public function getActiviteClient()
    {
        return $this->activiteClient;
    }

    /**
     * Set opttech
     *
     * @param string $opttech
     *
     * @return VidsForm
     */
    public function setOpttech($opttech)
    {
        $this->opttech = implode(' / ',$opttech);

        return $opttech;
    }

    /**
     * Get opttech
     *
     * @return string
     */
    public function getOpttech()
    {
        $opttechTab = explode(' / ', $this->opttech);
        return $opttechTab;
    }

    /**
     * Set commercial
     *
     * @param string $commercial
     *
     * @return VidsForm
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial
     *
     * @return string
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Set datelivraison
     *
     * @param string $datelivraison
     *
     * @return VidsForm
     */
    public function setDatelivraison($datelivraison)
    {
        $this->datelivraison = $datelivraison;

        return $this;
    }

    /**
     * Get datelivraison
     *
     * @return string
     */
    public function getDatelivraison()
    {
        return $this->datelivraison;
    }

    /**
     * Set observations
     *
     * @param string $observations
     *
     * @return VidsForm
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return VidsForm
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set titreVideo
     *
     * @param string $titreVideo
     *
     * @return VidsForm
     */
    public function setTitreVideo($titreVideo)
    {
        $this->titreVideo = $titreVideo;

        return $this;
    }

    /**
     * Get titreVideo
     *
     * @return string
     */
    public function getTitreVideo()
    {
        return $this->titreVideo;
    }

    /**
     * Set numOrdre
     *
     * @param string $numOrdre
     *
     * @return VidsForm
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;

        return $this;
    }

    /**
     * Get numOrdre
     *
     * @return string
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * Set reseauTwitter
     *
     * @param string $reseauTwitter
     *
     * @return VidsForm
     */
    public function setReseauTwitter($reseauTwitter)
    {
        $this->reseauTwitter = $reseauTwitter;

        return $this;
    }

    /**
     * Get reseauTwitter
     *
     * @return string
     */
    public function getReseauTwitter()
    {
        return $this->reseauTwitter;
    }

    /**
     * Set reseauFacebook
     *
     * @param string $reseauFacebook
     *
     * @return VidsForm
     */
    public function setReseauFacebook($reseauFacebook)
    {
        $this->reseauFacebook = $reseauFacebook;

        return $this;
    }

    /**
     * Get reseauFacebook
     *
     * @return string
     */
    public function getReseauFacebook()
    {
        return $this->reseauFacebook;
    }

    /**
     * Set reseauLinkedin
     *
     * @param string $reseauLinkedin
     *
     * @return VidsForm
     */
    public function setReseauLinkedin($reseauLinkedin)
    {
        $this->reseauLinkedin = $reseauLinkedin;

        return $this;
    }

    /**
     * Get reseauLinkedin
     *
     * @return string
     */
    public function getReseauLinkedin()
    {
        return $this->reseauLinkedin;
    }

    /**
     * Set reseauYoutube
     *
     * @param string $reseauYoutube
     *
     * @return VidsForm
     */
    public function setReseauYoutube($reseauYoutube)
    {
        $this->reseauYoutube = $reseauYoutube;

        return $this;
    }

    /**
     * Get reseauYoutube
     *
     * @return string
     */
    public function getReseauYoutube()
    {
        return $this->reseauYoutube;
    }

    /**
     * Set reseauInsta
     *
     * @param string $reseauInsta
     *
     * @return VidsForm
     */
    public function setReseauInsta($reseauInsta)
    {
        $this->reseauInsta = $reseauInsta;

        return $this;
    }

    /**
     * Get reseauInsta
     *
     * @return string
     */
    public function getReseauInsta()
    {
        return $this->reseauInsta;
    }

    /**
     * Set reseauPinterest
     *
     * @param string $reseauPinterest
     *
     * @return VidsForm
     */
    public function setReseauPinterest($reseauPinterest)
    {
        $this->reseauPinterest = $reseauPinterest;

        return $this;
    }

    /**
     * Get reseauPinterest
     *
     * @return string
     */
    public function getReseauPinterest()
    {
        return $this->reseauPinterest;
    }

    /**
     * Set reseauViadeo
     *
     * @param string $reseauViadeo
     *
     * @return VidsForm
     */
    public function setReseauViadeo($reseauViadeo)
    {
        $this->reseauViadeo = $reseauViadeo;

        return $this;
    }

    /**
     * Get reseauViadeo
     *
     * @return string
     */
    public function getReseauViadeo()
    {
        return $this->reseauViadeo;
    }

    /**
     * Set reseauGoogle
     *
     * @param string $reseauGoogle
     *
     * @return VidsForm
     */
    public function setReseauGoogle($reseauGoogle)
    {
        $this->reseauGoogle = $reseauGoogle;

        return $this;
    }

    /**
     * Get reseauGoogle
     *
     * @return string
     */
    public function getReseauGoogle()
    {
        return $this->reseauGoogle;
    }

    /**
     * Set cible
     *
     * @param string $cible
     *
     * @return VidsForm
     */
    public function setCible($cible)
    {
        $this->cible = $cible;

        return $this;
    }

    /**
     * Get cible
     *
     * @return string
     */
    public function getCible()
    {
        return $this->cible;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return VidsForm
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }
}

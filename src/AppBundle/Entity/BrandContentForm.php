<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BrandContentForm
 *
 * @ORM\Table(name="brand_content_form")
 * @ORM\Entity
 *
 */
class BrandContentForm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="societe", type="string", length=150, nullable=true)
     */
    private $societe;


    /**
     * @var string
     *
     * @ORM\Column(name="nom_client", type="string", length=200, nullable=true)
     */
    private $nomClient;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_client", type="string", length=200, nullable=true)
     */
    private $mailClient;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_client", type="string", length=200, nullable=true)
     */
    private $telClient;

    /**
     * @var string
     *
     * @ORM\Column(name="site_web_client", type="string", length=200, nullable=true)
     */
    private $siteWebClient;

    /**
     * @var string
     *
     * @ORM\Column(name="commercial", type="string", length=255, nullable=true)
     */
    private $commercial;

    /**
     * @var string
     *
     * @ORM\Column(name="objectif", type="string", nullable=true)
     */
    private $objectif;


    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", nullable=true)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="arrondissement", type="string", nullable=true)
     */
    private $arrondissement;

    /**
     * @var string
     *
     * @ORM\Column(name="ville1", type="string", length=200, nullable=true)
     */
    private $ville1;

    /**
     * @var string
     *
     * @ORM\Column(name="ville2", type="string", length=200, nullable=true)
     */
    private $ville2;

    /**
     * @var string
     *
     * @ORM\Column(name="ville3", type="string", length=200, nullable=true)
     */
    private $ville3;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayon1", type="integer", nullable=true)
     */
    private $rayon1;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayon2", type="integer", nullable=true)
     */
    private $rayon2;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayon3", type="integer", nullable=true)
     */
    private $rayon3;

    /**
     * @var string
     *
     * @ORM\Column(name="region1", type="string", length=50, nullable=true)
     */
    private $region1;

    /**
     * @var string
     *
     * @ORM\Column(name="region2", type="string", length=50, nullable=true)
     */
    private $region2;

    /**
     * @var string
     *
     * @ORM\Column(name="region3", type="string", length=50, nullable=true)
     */
    private $region3;

    /**
     * @var integer
     *
     * @ORM\Column(name="agemin", type="integer", nullable=true)
     */
    private $agemin;

    /**
     * @var integer
     *
     * @ORM\Column(name="agemax", type="integer", nullable=true)
     */
    private $agemax;

    /**
     * @var string
     *
     * @ORM\Column(name="interet1", type="string", length=150, nullable=true)
     */
    private $interet1;

    /**
     * @var string
     *
     * @ORM\Column(name="interet2", type="string", length=150, nullable=true)
     */
    private $interet2;

    /**
     * @var string
     *
     * @ORM\Column(name="interet3", type="string", length=150, nullable=true)
     */
    private $interet3;

    /**
     * @var integer
     *
     * @ORM\Column(name="offre", type="integer", nullable=true)
     */
    private $offre;

    /**
     * @var string
     *
     * @ORM\Column(name="fbpage", type="string", length=150, nullable=true)
     */
    private $fbpage;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", length=65535, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", length=65535, nullable=true)
     */
    private $observations;


    /**
     * @var string
     *
     * @ORM\Column(name="num_ordre", type="string", length=50, nullable=true)
     */
    private $numOrdre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_diffusion", type="datetime", nullable=true)
     */
    private $dateDiffusion;


    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * @param string $societe
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;
    }

    /**
     * @return string
     */
    public function getNomClient()
    {
        return $this->nomClient;
    }

    /**
     * @param string $nomClient
     */
    public function setNomClient($nomClient)
    {
        $this->nomClient = $nomClient;
    }

    /**
     * @return string
     */
    public function getMailClient()
    {
        return $this->mailClient;
    }

    /**
     * @param string $mailClient
     */
    public function setMailClient($mailClient)
    {
        $this->mailClient = $mailClient;
    }

    /**
     * @return string
     */
    public function getTelClient()
    {
        return $this->telClient;
    }

    /**
     * @param string $telClient
     */
    public function setTelClient($telClient)
    {
        $this->telClient = $telClient;
    }

    /**
     * @return string
     */
    public function getSiteWebClient()
    {
        return $this->siteWebClient;
    }

    /**
     * @param string $siteWebClient
     */
    public function setSiteWebClient($siteWebClient)
    {
        $this->siteWebClient = $siteWebClient;
    }

    /**
     * @return string
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * @param string $commercial
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;
    }

    /**
     * @return string
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * @param string $objectif
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;
    }

    /**
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * @param string $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * @return string
     */
    public function getVille1()
    {
        return $this->ville1;
    }

    /**
     * @param string $ville1
     */
    public function setVille1($ville1)
    {
        $this->ville1 = $ville1;
    }

    /**
     * @return string
     */
    public function getVille2()
    {
        return $this->ville2;
    }

    /**
     * @param string $ville2
     */
    public function setVille2($ville2)
    {
        $this->ville2 = $ville2;
    }

    /**
     * @return string
     */
    public function getVille3()
    {
        return $this->ville3;
    }

    /**
     * @param string $ville3
     */
    public function setVille3($ville3)
    {
        $this->ville3 = $ville3;
    }

    /**
     * @return int
     */
    public function getRayon1()
    {
        return $this->rayon1;
    }

    /**
     * @param int $rayon1
     */
    public function setRayon1($rayon1)
    {
        $this->rayon1 = $rayon1;
    }

    /**
     * @return int
     */
    public function getRayon2()
    {
        return $this->rayon2;
    }

    /**
     * @param int $rayon2
     */
    public function setRayon2($rayon2)
    {
        $this->rayon2 = $rayon2;
    }

    /**
     * @return int
     */
    public function getRayon3()
    {
        return $this->rayon3;
    }

    /**
     * @param int $rayon3
     */
    public function setRayon3($rayon3)
    {
        $this->rayon3 = $rayon3;
    }

    /**
     * @return string
     */
    public function getRegion1()
    {
        return $this->region1;
    }

    /**
     * @param string $region1
     */
    public function setRegion1($region1)
    {
        $this->region1 = $region1;
    }

    /**
     * @return string
     */
    public function getRegion2()
    {
        return $this->region2;
    }

    /**
     * @param string $region2
     */
    public function setRegion2($region2)
    {
        $this->region2 = $region2;
    }

    /**
     * @return string
     */
    public function getRegion3()
    {
        return $this->region3;
    }

    /**
     * @param string $region3
     */
    public function setRegion3($region3)
    {
        $this->region3 = $region3;
    }

    /**
     * @return int
     */
    public function getAgemin()
    {
        return $this->agemin;
    }

    /**
     * @param int $agemin
     */
    public function setAgemin($agemin)
    {
        $this->agemin = $agemin;
    }

    /**
     * @return int
     */
    public function getAgemax()
    {
        return $this->agemax;
    }

    /**
     * @param int $agemax
     */
    public function setAgemax($agemax)
    {
        $this->agemax = $agemax;
    }

    /**
     * @return string
     */
    public function getInteret1()
    {
        return $this->interet1;
    }

    /**
     * @param string $interet1
     */
    public function setInteret1($interet1)
    {
        $this->interet1 = $interet1;
    }

    /**
     * @return string
     */
    public function getInteret2()
    {
        return $this->interet2;
    }

    /**
     * @param string $interet2
     */
    public function setInteret2($interet2)
    {
        $this->interet2 = $interet2;
    }

    /**
     * @return string
     */
    public function getInteret3()
    {
        return $this->interet3;
    }

    /**
     * @param string $interet3
     */
    public function setInteret3($interet3)
    {
        $this->interet3 = $interet3;
    }

    /**
     * @return int
     */
    public function getOffre()
    {
        return $this->offre;
    }

    /**
     * @param int $offre
     */
    public function setOffre($offre)
    {
        $this->offre = $offre;
    }

    /**
     * @return string
     */
    public function getFbpage()
    {
        return $this->fbpage;
    }

    /**
     * @param string $fbpage
     */
    public function setFbpage($fbpage)
    {
        $this->fbpage = $fbpage;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * @param string $observations
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;
    }

    /**
     * @return string
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * @param string $numOrdre
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;
    }



    /**
     * Set arrondissement
     *
     * @param string $arrondissement
     *
     * @return FacebookForm
     */
    public function setArrondissement($arrondissement)
    {
        $this->arrondissement = implode(' / ',$arrondissement);

        return $this;
    }

    /**
     * Get arrondissement
     *
     * @return string
     */
    public function getArrondissement()
    {
        $arrondissementTab = explode(' / ', $this->arrondissement);
        return $arrondissementTab;
    }

    /**
     * @return \DateTime
     */
    public function getDateDiffusion()
    {
        return $this->dateDiffusion;
    }

    /**
     * @param \DateTime $dateDiffusion
     */
    public function setDateDiffusion($dateDiffusion)
    {
        $this->dateDiffusion = $dateDiffusion;
    }


}

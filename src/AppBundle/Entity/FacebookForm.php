<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * FacebookForm
 *
 * @ORM\Table(name="facebook_form")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FacebookFormRepository")
 *
 */
class FacebookForm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="arrondissement", type="string", nullable=true)
     */
    private $arrondissement;

    /**
     * @var string
     *
     * @ORM\Column(name="entry_type", type="string", length=15, nullable=false)
     */
    private $entryType;

    /**
     * @var string
     *
     * @ORM\Column(name="societe", type="string", length=150, nullable=true)
     */
    private $societe;

    /**
     * @var string
     *
     * @ORM\Column(name="siteweb_client", type="string", length=255, nullable=true)
     */
    private $sitewebClient;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_client", type="string", length=200, nullable=true)
     */
    private $nomClient;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_client", type="string", length=255, nullable=true)
     */
    private $mailClient;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_client", type="string", length=15, nullable=true)
     */
    private $telClient;

    /**
     * @var string
     *
     * @ORM\Column(name="page_client", type="string", length=255, nullable=true)
     */
    private $pageClient;

    /**
     * @var string
     *
     * @ORM\Column(name="pageclientpourmaj", type="string", length=255, nullable=true)
     */
    private $pageclientpourmaj;

    /**
     * @var string
     *
     * @ORM\Column(name="activite_client", type="string", length=255, nullable=true)
     */
    private $activiteClient;

    /**
     * @var string
     *
     * @ORM\Column(name="horaires_client", type="string", length=255, nullable=true)
     */
    private $horairesClient;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_btn", type="integer", nullable=true)
     */
    private $typeBtn;

    /**
     * @var string
     *
     * @ORM\Column(name="url_redirect", type="string", length=255, nullable=true)
     */
    private $urlRedirect;

    /**
     * @var string
     *
     * @ORM\Column(name="commercial", type="string", length=255, nullable=true)
     */
    private $commercial;

    /**
     * @var string
     *
     * @ORM\Column(name="objectif", type="string", nullable=true)
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="datecampagne", type="string", length=50, nullable=true)
     */
    private $datecampagne;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", nullable=true)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="ville1", type="string", length=200, nullable=true)
     */
    private $ville1;

    /**
     * @var string
     *
     * @ORM\Column(name="ville2", type="string", length=200, nullable=true)
     */
    private $ville2;

    /**
     * @var string
     *
     * @ORM\Column(name="ville3", type="string", length=200, nullable=true)
     */
    private $ville3;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayon1", type="integer", nullable=true)
     */
    private $rayon1;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayon2", type="integer", nullable=true)
     */
    private $rayon2;

    /**
     * @var integer
     *
     * @ORM\Column(name="rayon3", type="integer", nullable=true)
     */
    private $rayon3;

    /**
     * @var string
     *
     * @ORM\Column(name="region1", type="string", length=50, nullable=true)
     */
    private $region1;

    /**
     * @var string
     *
     * @ORM\Column(name="region2", type="string", length=50, nullable=true)
     */
    private $region2;

    /**
     * @var string
     *
     * @ORM\Column(name="region3", type="string", length=50, nullable=true)
     */
    private $region3;

    /**
     * @var integer
     *
     * @ORM\Column(name="agemin", type="integer", nullable=true)
     */
    private $agemin;

    /**
     * @var integer
     *
     * @ORM\Column(name="agemax", type="integer", nullable=true)
     */
    private $agemax;

    /**
     * @var string
     *
     * @ORM\Column(name="interet1", type="string", length=150, nullable=true)
     */
    private $interet1;

    /**
     * @var string
     *
     * @ORM\Column(name="interet2", type="string", length=150, nullable=true)
     */
    private $interet2;

    /**
     * @var string
     *
     * @ORM\Column(name="interet3", type="string", length=150, nullable=true)
     */
    private $interet3;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="offre", type="integer", nullable=true)
     */
    private $offre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="fbpage", type="boolean", nullable=true)
     */
    private $fbpage;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text", length=65535, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", length=65535, nullable=true)
     */
    private $observations;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_campagne", type="string", length=255, nullable=true)
     */
    private $titreCampagne;

    /**
     * @var string
     *
     * @ORM\Column(name="num_ordre", type="string", length=50, nullable=true)
     */
    private $numOrdre;

    /**
     * @var string
     *
     * @ORM\Column(name="clientfblogin", type="text", length=65535, nullable=true)
     */
    private $clientfblogin;

    /**
     * @var string
     *
     * @ORM\Column(name="clientfbpwd", type="text", length=65535, nullable=true)
     */
    private $clientfbpwd;


    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return FacebookForm
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set arrondissement
     *
     * @param string $arrondissement
     *
     * @return FacebookForm
     */
    public function setArrondissement($arrondissement)
    {
        $this->arrondissement = implode(' / ',$arrondissement);

        return $this;
    }

    /**
     * Get arrondissement
     *
     * @return string
     */
    public function getArrondissement()
    {
        $arrondissementTab = explode(' / ', $this->arrondissement);
        return $arrondissementTab;
    }

    /**
     * Set entryType
     *
     * @param string $entryType
     *
     * @return FacebookForm
     */
    public function setEntryType($entryType)
    {
        $this->entryType = $entryType;

        return $this;
    }

    /**
     * Get entryType
     *
     * @return string
     */
    public function getEntryType()
    {
        return $this->entryType;
    }

    /**
     * Set societe
     *
     * @param string $societe
     *
     * @return FacebookForm
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;

        return $this;
    }

    /**
     * Get societe
     *
     * @return string
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * Set sitewebClient
     *
     * @param string $sitewebClient
     *
     * @return FacebookForm
     */
    public function setSitewebClient($sitewebClient)
    {
        $this->sitewebClient = $sitewebClient;

        return $this;
    }

    /**
     * Get sitewebClient
     *
     * @return string
     */
    public function getSitewebClient()
    {
        return $this->sitewebClient;
    }

    /**
     * Set nomClient
     *
     * @param string $nomClient
     *
     * @return FacebookForm
     */
    public function setNomClient($nomClient)
    {
        $this->nomClient = $nomClient;

        return $this;
    }

    /**
     * Get nomClient
     *
     * @return string
     */
    public function getNomClient()
    {
        return $this->nomClient;
    }

    /**
     * Set mailClient
     *
     * @param string $mailClient
     *
     * @return FacebookForm
     */
    public function setMailClient($mailClient)
    {
        $this->mailClient = $mailClient;

        return $this;
    }

    /**
     * Get mailClient
     *
     * @return string
     */
    public function getMailClient()
    {
        return $this->mailClient;
    }

    /**
     * Set telClient
     *
     * @param string $telClient
     *
     * @return FacebookForm
     */
    public function setTelClient($telClient)
    {
        $this->telClient = $telClient;

        return $this;
    }

    /**
     * Get telClient
     *
     * @return string
     */
    public function getTelClient()
    {
        return $this->telClient;
    }

    /**
     * Set pageClient
     *
     * @param string $pageClient
     *
     * @return FacebookForm
     */
    public function setPageClient($pageClient)
    {
        $this->pageClient = $pageClient;

        return $this;
    }

    /**
     * Get pageClient
     *
     * @return string
     */
    public function getPageClient()
    {
        return $this->pageClient;
    }

    /**
     * Set pageclientpourmaj
     *
     * @param string $pageclientpourmaj
     *
     * @return FacebookForm
     */
    public function setPageclientpourmaj($pageclientpourmaj)
    {
        $this->pageclientpourmaj = $pageclientpourmaj;

        return $this;
    }

    /**
     * Get pageclientpourmaj
     *
     * @return string
     */
    public function getPageclientpourmaj()
    {
        return $this->pageclientpourmaj;
    }

    /**
     * Set activiteClient
     *
     * @param string $activiteClient
     *
     * @return FacebookForm
     */
    public function setActiviteClient($activiteClient)
    {
        $this->activiteClient = $activiteClient;

        return $this;
    }

    /**
     * Get activiteClient
     *
     * @return string
     */
    public function getActiviteClient()
    {
        return $this->activiteClient;
    }

    /**
     * Set horairesClient
     *
     * @param string $horairesClient
     *
     * @return FacebookForm
     */
    public function setHorairesClient($horairesClient)
    {
        $this->horairesClient = $horairesClient;

        return $this;
    }

    /**
     * Get horairesClient
     *
     * @return string
     */
    public function getHorairesClient()
    {
        return $this->horairesClient;
    }

    /**
     * Set typeBtn
     *
     * @param integer $typeBtn
     *
     * @return FacebookForm
     */
    public function setTypeBtn($typeBtn)
    {
        $this->typeBtn = $typeBtn;

        return $this;
    }

    /**
     * Get typeBtn
     *
     * @return integer
     */
    public function getTypeBtn()
    {
        return $this->typeBtn;
    }

    /**
     * Set urlRedirect
     *
     * @param string $urlRedirect
     *
     * @return FacebookForm
     */
    public function setUrlRedirect($urlRedirect)
    {
        $this->urlRedirect = $urlRedirect;

        return $this;
    }

    /**
     * Get urlRedirect
     *
     * @return string
     */
    public function getUrlRedirect()
    {
        return $this->urlRedirect;
    }

    /**
     * Set commercial
     *
     * @param string $commercial
     *
     * @return FacebookForm
     */
    public function setCommercial($commercial)
    {
        $this->commercial = $commercial;

        return $this;
    }

    /**
     * Get commercial
     *
     * @return string
     */
    public function getCommercial()
    {
        return $this->commercial;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     *
     * @return FacebookForm
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set datecampagne
     *
     * @param string $datecampagne
     *
     * @return FacebookForm
     */
    public function setDatecampagne($datecampagne)
    {
        $this->datecampagne = $datecampagne;

        return $this;
    }

    /**
     * Get datecampagne
     *
     * @return string
     */
    public function getDatecampagne()
    {
        return $this->datecampagne;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return FacebookForm
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }


    /**
     * Set ville1
     *
     * @param string $ville1
     *
     * @return FacebookForm
     */
    public function setVille1($ville1)
    {
        $this->ville1 = $ville1;

        return $this;
    }

    /**
     * Get ville1
     *
     * @return string
     */
    public function getVille1()
    {
        return $this->ville1;
    }

    /**
     * Set ville2
     *
     * @param string $ville2
     *
     * @return FacebookForm
     */
    public function setVille2($ville2)
    {
        $this->ville2 = $ville2;

        return $this;
    }

    /**
     * Get ville2
     *
     * @return string
     */
    public function getVille2()
    {
        return $this->ville2;
    }

    /**
     * Set ville3
     *
     * @param string $ville3
     *
     * @return FacebookForm
     */
    public function setVille3($ville3)
    {
        $this->ville3 = $ville3;

        return $this;
    }

    /**
     * Get ville3
     *
     * @return string
     */
    public function getVille3()
    {
        return $this->ville3;
    }

    /**
     * Set rayon1
     *
     * @param integer $rayon1
     *
     * @return FacebookForm
     */
    public function setRayon1($rayon1)
    {
        $this->rayon1 = $rayon1;

        return $this;
    }

    /**
     * Get rayon1
     *
     * @return integer
     */
    public function getRayon1()
    {
        return $this->rayon1;
    }

    /**
     * Set rayon2
     *
     * @param integer $rayon2
     *
     * @return FacebookForm
     */
    public function setRayon2($rayon2)
    {
        $this->rayon2 = $rayon2;

        return $this;
    }

    /**
     * Get rayon2
     *
     * @return integer
     */
    public function getRayon2()
    {
        return $this->rayon2;
    }

    /**
     * Set rayon3
     *
     * @param integer $rayon3
     *
     * @return FacebookForm
     */
    public function setRayon3($rayon3)
    {
        $this->rayon3 = $rayon3;

        return $this;
    }

    /**
     * Get rayon3
     *
     * @return integer
     */
    public function getRayon3()
    {
        return $this->rayon3;
    }

    /**
     * Set region1
     *
     * @param string $region1
     *
     * @return FacebookForm
     */
    public function setRegion1($region1)
    {
        $this->region1 = $region1;

        return $this;
    }

    /**
     * Get region1
     *
     * @return string
     */
    public function getRegion1()
    {
        return $this->region1;
    }

    /**
     * Set region2
     *
     * @param string $region2
     *
     * @return FacebookForm
     */
    public function setRegion2($region2)
    {
        $this->region2 = $region2;

        return $this;
    }

    /**
     * Get region2
     *
     * @return string
     */
    public function getRegion2()
    {
        return $this->region2;
    }

    /**
     * Set region3
     *
     * @param string $region3
     *
     * @return FacebookForm
     */
    public function setRegion3($region3)
    {
        $this->region3 = $region3;

        return $this;
    }

    /**
     * Get region3
     *
     * @return string
     */
    public function getRegion3()
    {
        return $this->region3;
    }

    /**
     * Set agemin
     *
     * @param integer $agemin
     *
     * @return FacebookForm
     */
    public function setAgemin($agemin)
    {
        $this->agemin = $agemin;

        return $this;
    }

    /**
     * Get agemin
     *
     * @return integer
     */
    public function getAgemin()
    {
        return $this->agemin;
    }

    /**
     * Set agemax
     *
     * @param integer $agemax
     *
     * @return FacebookForm
     */
    public function setAgemax($agemax)
    {
        $this->agemax = $agemax;

        return $this;
    }

    /**
     * Get agemax
     *
     * @return integer
     */
    public function getAgemax()
    {
        return $this->agemax;
    }

    /**
     * Set interet1
     *
     * @param string $interet1
     *
     * @return FacebookForm
     */
    public function setInteret1($interet1)
    {
        $this->interet1 = $interet1;

        return $this;
    }

    /**
     * Get interet1
     *
     * @return string
     */
    public function getInteret1()
    {
        return $this->interet1;
    }

    /**
     * Set interet2
     *
     * @param string $interet2
     *
     * @return FacebookForm
     */
    public function setInteret2($interet2)
    {
        $this->interet2 = $interet2;

        return $this;
    }

    /**
     * Get interet2
     *
     * @return string
     */
    public function getInteret2()
    {
        return $this->interet2;
    }

    /**
     * Set interet3
     *
     * @param string $interet3
     *
     * @return FacebookForm
     */
    public function setInteret3($interet3)
    {
        $this->interet3 = $interet3;

        return $this;
    }

    /**
     * Get interet3
     *
     * @return string
     */
    public function getInteret3()
    {
        return $this->interet3;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return FacebookForm
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set offre
     *
     * @param integer $offre
     *
     * @return FacebookForm
     */
    public function setOffre($offre)
    {
        $this->offre = $offre;

        return $this;
    }

    /**
     * Get offre
     *
     * @return integer
     */
    public function getOffre()
    {
        return $this->offre;
    }

    /**
     * Set fbpage
     *
     * @param boolean $fbpage
     *
     * @return FacebookForm
     */
    public function setFbpage($fbpage)
    {
        $this->fbpage = $fbpage;

        return $this;
    }

    /**
     * Get fbpage
     *
     * @return boolean
     */
    public function getFbpage()
    {
        return $this->fbpage;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return FacebookForm
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set observations
     *
     * @param string $observations
     *
     * @return FacebookForm
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set titreCampagne
     *
     * @param string $titreCampagne
     *
     * @return FacebookForm
     */
    public function setTitreCampagne($titreCampagne)
    {
        $this->titreCampagne = $titreCampagne;

        return $this;
    }

    /**
     * Get titreCampagne
     *
     * @return string
     */
    public function getTitreCampagne()
    {
        return $this->titreCampagne;
    }

    /**
     * Set numOrdre
     *
     * @param string $numOrdre
     *
     * @return FacebookForm
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;

        return $this;
    }

    /**
     * Get numOrdre
     *
     * @return string
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * Set clientfblogin
     *
     * @param string $clientfblogin
     *
     * @return FacebookForm
     */
    public function setClientfblogin($clientfblogin)
    {
        $this->clientfblogin = $clientfblogin;

        return $this;
    }

    /**
     * Get clientfblogin
     *
     * @return string
     */
    public function getClientfblogin()
    {
        return $this->clientfblogin;
    }

    /**
     * Set clientfbpwd
     *
     * @param string $clientfbpwd
     *
     * @return FacebookForm
     */
    public function setClientfbpwd($clientfbpwd)
    {
        $this->clientfbpwd = $clientfbpwd;

        return $this;
    }

    /**
     * Get clientfbpwd
     *
     * @return string
     */
    public function getClientfbpwd()
    {
        return $this->clientfbpwd;
    }
}

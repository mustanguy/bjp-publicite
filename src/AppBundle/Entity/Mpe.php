<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mpe
 *
 * @ORM\Table(name="mpe")
 * @ORM\Entity
 */
class Mpe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_pub", type="datetime", nullable=false)
     */
    private $datePub;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=250, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="description_longue", type="text", length=65535, nullable=true)
     */
    private $descriptionLongue;

    /**
     * @var string
     *
     * @ORM\Column(name="chapo", type="text", length=65535, nullable=false)
     */
    private $chapo;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=250, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=false)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=false)
     */
    private $site;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_secteur", type="integer", nullable=false)
     */
    private $idSecteur;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_entreprise", type="string", length=200, nullable=false)
     */
    private $nomEntreprise;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=false)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=6, nullable=false)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=100, nullable=false)
     */
    private $ville;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bonplan", type="boolean", nullable=false)
     */
    private $bonplan;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="tel1", type="string", length=30, nullable=false)
     */
    private $tel1;

    /**
     * @var string
     *
     * @ORM\Column(name="tel2", type="string", length=30, nullable=false)
     */
    private $tel2;

    /**
     * @var string
     *
     * @ORM\Column(name="embed_1", type="text", length=65535, nullable=true)
     */
    private $embed1;

    /**
     * @var string
     *
     * @ORM\Column(name="embed_2", type="text", length=65535, nullable=true)
     */
    private $embed2;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", length=255, nullable=false)
     */
    private $thumb;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=100, nullable=false)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=100, nullable=false)
     */
    private $lng;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datePub
     *
     * @param \DateTime $datePub
     *
     * @return Mpe
     */
    public function setDatePub($datePub)
    {
        $this->datePub = $datePub;

        return $this;
    }

    /**
     * Get datePub
     *
     * @return \DateTime
     */
    public function getDatePub()
    {
        return $this->datePub;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Mpe
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Mpe
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set descriptionLongue
     *
     * @param string $descriptionLongue
     *
     * @return Mpe
     */
    public function setDescriptionLongue($descriptionLongue)
    {
        $this->descriptionLongue = $descriptionLongue;

        return $this;
    }

    /**
     * Get descriptionLongue
     *
     * @return string
     */
    public function getDescriptionLongue()
    {
        return $this->descriptionLongue;
    }

    /**
     * Set chapo
     *
     * @param string $chapo
     *
     * @return Mpe
     */
    public function setChapo($chapo)
    {
        $this->chapo = $chapo;

        return $this;
    }

    /**
     * Get chapo
     *
     * @return string
     */
    public function getChapo()
    {
        return $this->chapo;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return Mpe
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return Mpe
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Mpe
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set idSecteur
     *
     * @param integer $idSecteur
     *
     * @return Mpe
     */
    public function setIdSecteur($idSecteur)
    {
        $this->idSecteur = $idSecteur;

        return $this;
    }

    /**
     * Get idSecteur
     *
     * @return integer
     */
    public function getIdSecteur()
    {
        return $this->idSecteur;
    }

    /**
     * Set nomEntreprise
     *
     * @param string $nomEntreprise
     *
     * @return Mpe
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * Get nomEntreprise
     *
     * @return string
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Mpe
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cp
     *
     * @param string $cp
     *
     * @return Mpe
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Mpe
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set bonplan
     *
     * @param boolean $bonplan
     *
     * @return Mpe
     */
    public function setBonplan($bonplan)
    {
        $this->bonplan = $bonplan;

        return $this;
    }

    /**
     * Get bonplan
     *
     * @return boolean
     */
    public function getBonplan()
    {
        return $this->bonplan;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Mpe
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tel1
     *
     * @param string $tel1
     *
     * @return Mpe
     */
    public function setTel1($tel1)
    {
        $this->tel1 = $tel1;

        return $this;
    }

    /**
     * Get tel1
     *
     * @return string
     */
    public function getTel1()
    {
        return $this->tel1;
    }

    /**
     * Set tel2
     *
     * @param string $tel2
     *
     * @return Mpe
     */
    public function setTel2($tel2)
    {
        $this->tel2 = $tel2;

        return $this;
    }

    /**
     * Get tel2
     *
     * @return string
     */
    public function getTel2()
    {
        return $this->tel2;
    }

    /**
     * Set embed1
     *
     * @param string $embed1
     *
     * @return Mpe
     */
    public function setEmbed1($embed1)
    {
        $this->embed1 = $embed1;

        return $this;
    }

    /**
     * Get embed1
     *
     * @return string
     */
    public function getEmbed1()
    {
        return $this->embed1;
    }

    /**
     * Set embed2
     *
     * @param string $embed2
     *
     * @return Mpe
     */
    public function setEmbed2($embed2)
    {
        $this->embed2 = $embed2;

        return $this;
    }

    /**
     * Get embed2
     *
     * @return string
     */
    public function getEmbed2()
    {
        return $this->embed2;
    }

    /**
     * Set thumb
     *
     * @param string $thumb
     *
     * @return Mpe
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb
     *
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return Mpe
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return Mpe
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }
}

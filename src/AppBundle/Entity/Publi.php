<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Publi
 *
 * @ORM\Table(name="publi")
 * @ORM\Entity
 */
class Publi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="departement", type="integer", nullable=false)
     */
    private $departement;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_commercial", type="string", length=60, nullable=false)
     */
    private $nomCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_commercial", type="string", length=15, nullable=false)
     */
    private $telCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="email_commercial", type="string", length=150, nullable=false)
     */
    private $emailCommercial;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_annonceur", type="string", length=100, nullable=true)
     */
    private $nomAnnonceur;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_annonceur", type="string", length=255, nullable=false)
     */
    private $contactAnnonceur;

    /**
     * @var string
     *
     * @ORM\Column(name="tel_annonceur", type="string", length=20, nullable=false)
     */
    private $telAnnonceur;

    /**
     * @var string
     *
     * @ORM\Column(name="email_annonceur", type="string", length=150, nullable=false)
     */
    private $emailAnnonceur;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_annonceur", type="string", length=255, nullable=false)
     */
    private $adresseAnnonceur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_saisie", type="datetime", nullable=false)
     */
    private $dateSaisie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_parution", type="datetime", nullable=false)
     */
    private $dateParution;

    /**
     * @var string
     *
     * @ORM\Column(name="support", type="string", length=255, nullable=false)
     */
    private $support;

    /**
     * @var integer
     *
     * @ORM\Column(name="format", type="integer", nullable=false)
     */
    private $format;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255, nullable=true)
     */
    private $theme;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", length=65535, nullable=true)
     */
    private $observations;

    /**
     * @var string
     *
     * @ORM\Column(name="raisonsocial_annonceur", type="string", length=255, nullable=false)
     */
    private $raisonsocialAnnonceur;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    public function __construct()
    {
        $this->dateSaisie = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set departement
     *
     * @param integer $departement
     *
     * @return Publi
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return integer
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set nomCommercial
     *
     * @param string $nomCommercial
     *
     * @return Publi
     */
    public function setNomCommercial($nomCommercial)
    {
        $this->nomCommercial = $nomCommercial;

        return $this;
    }

    /**
     * Get nomCommercial
     *
     * @return string
     */
    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    /**
     * Set telCommercial
     *
     * @param string $telCommercial
     *
     * @return Publi
     */
    public function setTelCommercial($telCommercial)
    {
        $this->telCommercial = $telCommercial;

        return $this;
    }

    /**
     * Get telCommercial
     *
     * @return string
     */
    public function getTelCommercial()
    {
        return $this->telCommercial;
    }

    /**
     * Set emailCommercial
     *
     * @param string $emailCommercial
     *
     * @return Publi
     */
    public function setEmailCommercial($emailCommercial)
    {
        $this->emailCommercial = $emailCommercial;

        return $this;
    }

    /**
     * Get emailCommercial
     *
     * @return string
     */
    public function getEmailCommercial()
    {
        return $this->emailCommercial;
    }

    /**
     * Set nomAnnonceur
     *
     * @param string $nomAnnonceur
     *
     * @return Publi
     */
    public function setNomAnnonceur($nomAnnonceur)
    {
        $this->nomAnnonceur = $nomAnnonceur;

        return $this;
    }

    /**
     * Get nomAnnonceur
     *
     * @return string
     */
    public function getNomAnnonceur()
    {
        return $this->nomAnnonceur;
    }

    /**
     * Set contactAnnonceur
     *
     * @param string $contactAnnonceur
     *
     * @return Publi
     */
    public function setContactAnnonceur($contactAnnonceur)
    {
        $this->contactAnnonceur = $contactAnnonceur;

        return $this;
    }

    /**
     * Get contactAnnonceur
     *
     * @return string
     */
    public function getContactAnnonceur()
    {
        return $this->contactAnnonceur;
    }

    /**
     * Set telAnnonceur
     *
     * @param string $telAnnonceur
     *
     * @return Publi
     */
    public function setTelAnnonceur($telAnnonceur)
    {
        $this->telAnnonceur = $telAnnonceur;

        return $this;
    }

    /**
     * Get telAnnonceur
     *
     * @return string
     */
    public function getTelAnnonceur()
    {
        return $this->telAnnonceur;
    }

    /**
     * Set emailAnnonceur
     *
     * @param string $emailAnnonceur
     *
     * @return Publi
     */
    public function setEmailAnnonceur($emailAnnonceur)
    {
        $this->emailAnnonceur = $emailAnnonceur;

        return $this;
    }

    /**
     * Get emailAnnonceur
     *
     * @return string
     */
    public function getEmailAnnonceur()
    {
        return $this->emailAnnonceur;
    }

    /**
     * Set adresseAnnonceur
     *
     * @param string $adresseAnnonceur
     *
     * @return Publi
     */
    public function setAdresseAnnonceur($adresseAnnonceur)
    {
        $this->adresseAnnonceur = $adresseAnnonceur;

        return $this;
    }

    /**
     * Get adresseAnnonceur
     *
     * @return string
     */
    public function getAdresseAnnonceur()
    {
        return $this->adresseAnnonceur;
    }

    /**
     * Set dateSaisie
     *
     * @param \DateTime $dateSaisie
     *
     * @return Publi
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get dateSaisie
     *
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set dateParution
     *
     * @param \DateTime $dateParution
     *
     * @return Publi
     */
    public function setDateParution($dateParution)
    {
        $this->dateParution = $dateParution;

        return $this;
    }

    /**
     * Get dateParution
     *
     * @return \DateTime
     */
    public function getDateParution()
    {
        return $this->dateParution;
    }

    /**
     * Set support
     *
     * @param string $support
     *
     * @return Publi
     */
    public function setSupport($support)
    {
        $this->support = implode('@',$support);

        return $this;
    }

    /**
     * Get support
     *
     * @return string
     */
    public function getSupport()
    {
        $supportTab = explode('@', $this->support);
        return $supportTab;
    }

    /**
     * Set format
     *
     * @param integer $format
     *
     * @return Publi
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return integer
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Publi
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set observations
     *
     * @param string $observations
     *
     * @return Publi
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return string
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set raisonsocialAnnonceur
     *
     * @param string $raisonsocialAnnonceur
     *
     * @return Publi
     */
    public function setRaisonsocialAnnonceur($raisonsocialAnnonceur)
    {
        $this->raisonsocialAnnonceur = $raisonsocialAnnonceur;

        return $this;
    }

    /**
     * Get raisonsocialAnnonceur
     *
     * @return string
     */
    public function getRaisonsocialAnnonceur()
    {
        return $this->raisonsocialAnnonceur;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Publi
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}

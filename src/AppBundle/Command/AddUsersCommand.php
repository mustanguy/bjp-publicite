<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AddUsersCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('add-users')
            ->setDescription('Add liste of user')
            ->addArgument('list',InputArgument::REQUIRED, 'List of user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        $list = $input->getArgument('list');

        $utilisateurs = array();
        $row = 0;
        // Import du fichier CSV
        if (($handle = fopen($list, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                $data = array_map("utf8_encode", $data);
                $num = count($data);
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    $utilisateurs[$row] = array(
                        "nom" => $data[0],
                        "prenom" => $data[1],
                        "mail" => $data[2],
                        "role" => $data[3],
                    );
                }
            }
            fclose($handle);
        }

        $this->container = $this->getApplication()->getKernel()->getContainer();
        $userManager = $this->container->get('fos_user.user_manager');

        $i = 0;
        foreach ($utilisateurs as $utilisateur) {
            $email_exist = $userManager->findUserByEmail($utilisateur["mail"]);

            if(!$email_exist){
                $user = $userManager->createUser();
                $user->setUsername($utilisateur["mail"]);
                $user->setEmail($utilisateur["mail"]);
                $user->setEmailCanonical($utilisateur["mail"]);
                $user->setEnabled(1);
                $user->setPlainPassword('lprlbpjsl18');
                $user->addRole($utilisateur['role']);
                $user->setFirstname($utilisateur["prenom"]);
                $user->setLastname($utilisateur["nom"]);
                $userManager->updateUser($user);

                $i++;
            }
        }

        $output->writeln("====================================");
        $output->writeln($i . " utilisateurs ajoutés !!!");
        $output->writeln("====================================");
    }
}
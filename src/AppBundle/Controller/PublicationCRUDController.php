<?php


namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

class PublicationCRUDController extends CRUDController
{
    public function listAction()
    {
        $publications = $this->getDoctrine()->getRepository('AppBundle:Publi')->findBy([], ['dateSaisie' => 'DESC']);

        return $this->renderWithExtraParams('publication/list.html.twig', ['publications' => $publications]);
    }
}
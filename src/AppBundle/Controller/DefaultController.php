<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->redirectToRoute('sonata_admin_dashboard');
    }

    /**
     * @Route("/", name="get_document_link")
     */
    public function getDocumentAction(Request $request)
    {
        $documents = $this->getDoctrine()->getRepository('AppBundle:Documents')->findAll();
        $documentsCategory = $this->getDoctrine()->getRepository('AppBundle:DocumentsCategory')->findAll();

        return $this->render('default/menu_document.html.twig', ['documents' => $documents, 'documentsCategory' => $documentsCategory]);
    }

    /**
     * @Route("/publication/{id}/details", name="summary_publication")
     */
    public function summaryPublicationAction($id)
    {
        $publication = $this->getDoctrine()->getRepository('AppBundle:Publi')->find($id);

        return $this->render('publication/summary.html.twig', ['publication' => $publication]);
    }

    /**
     * @Route("/change/{publicationId}/status/{statusId}", name="change_status_publication")
     */
    public function changeStatusPublicationAction($publicationId, $statusId)
    {
        $publication = $this->getDoctrine()->getRepository('AppBundle:Publi')->find($publicationId);
        $publication->setStatus($statusId);

        return $this->redirectToRoute('summary_publication', ['id' => $publication->getId()]);
    }

    /**
     * @Route("/publish/{id}", name="publish_video")
     */
    public function publishVideoAction($id)
    {
        $video = $this->getDoctrine()->getRepository('AppBundle:Mapetiteentreprise')->find($id);
        $video->setStatus(true);
        $this->getDoctrine()->getManager()->persist($video);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_app_mapetiteentreprise_form_list');
    }

    /**
     * @Route("/depublish/{id}", name="depublish_video")
     */
    public function dePublishVideoAction($id)
    {
        $video = $this->getDoctrine()->getRepository('AppBundle:Mapetiteentreprise')->find($id);
        $video->setStatus(false);
        $this->getDoctrine()->getManager()->persist($video);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_app_mapetiteentreprise_form_list');
    }

    /**
     * @Route("/video/{id}/details", name="summary_video")
     */
    public function summaryVideoAction($id)
    {
        $video = $this->getDoctrine()->getRepository('AppBundle:VidsForm')->find($id);

        return $this->render('video/summary.html.twig', ['video' => $video]);
    }

}

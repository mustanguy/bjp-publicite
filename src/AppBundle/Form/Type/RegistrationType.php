<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,['attr'=> ['placeholder' => "Email", 'class' => 'form-control']])
            ->add('firstname', TextType::class,['attr'=> ['placeholder' => "Prénom", 'class' => 'form-control']])
            ->add('lastname', TextType::class,['attr'=> ['placeholder' => "Nom", 'class' => 'form-control']])
            ->remove('plainPassword')
            ->remove('username');

    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
}
<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FileManagerType extends AbstractType
{

    public function getName()
    {
        return 'file_manager';
    }

    public function getParent()
    {
        return TextType::class;
    }
}
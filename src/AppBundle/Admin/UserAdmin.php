<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_user';

    protected $baseRoutePattern = 'admin_app_user';

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('email', EmailType::class, [
            'required' => true,
            'attr' => [
                "placeholder" => "Email",
            ]
        ])->add('firstname', TextType::class,[
            'required' => false,
            'attr' => [
                "placeholder" => "Prénom",
            ]
        ])->add('lastname', TextType::class, [
            'attr' => [
                "placeholder" => "Nom",
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email')
            ->add('firstname', null,['label' => 'Prénom'])
            ->add('lastname', null,['label' => 'Nom'])
            ->add('_action', null, [
            'actions' => [
                'edit' => [],
                'delete' => [],
            ]
        ]);
    }
}
<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\DateTime;

class PublicationFormAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_publication_form';

    protected $baseRoutePattern = 'admin_app_publication_form';


    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Demande de publi-rédactionnel";
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $dateParrutionMin = new \DateTime();
        $dateParrutionMinModified = $dateParrutionMin->modify('-4 days');

        $formMapper->add('departement', ChoiceType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Département",
            ],
            'choices' => [
                'Ain' => '1',
                'Loire / Haute-Loire' => '2',
                'Jura' => '3',
                'Rhône' => '4',
                'Saône et Loire' => '5',
                "Côte d'or" => '6',
            ]
        ])->add('nomCommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Nom du commercial',
            ]
        ])->add('telCommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Téléphone du commercial",
            ]
        ])->add('emailCommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Email du commercial",
            ]
        ])->add('raisonsocialAnnonceur', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Raison sociale / marque de l'annonceur *",
            ]
        ])->add('contactAnnonceur', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Nom du contact",
            ]
        ])->add('adresseAnnonceur', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Adresse de l'annonceur",
            ]
        ])->add('telAnnonceur', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Téléphone de l'annonceur",
            ]
        ])->add('emailAnnonceur', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "E-mail de l'annonceur",
            ]
        ])->add('dateParution', DatePickerType::class, [
            'required' => true,
            'format' => 'dd/MM/yyyy',
            'dp_min_date' => ($dateParrutionMinModified),

        ])->add('support', ChoiceType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Choix du support",
            ],
            'choices' => [
                'Le Progrès' => '1',
                'CNews Matin / Lyon Plus' => '2',
                'Le Bien Public' => '3',
                'Le Journal de Saône-et-Loire' => '4',
                'TV Mag' => '5',
                "Version Femina" => '6',
                "C'est en ville Lyon" => '7',
                "C'est en ville Est" => '8',
                "C'est en ville Ouest" => '9',
                "C'est en ville Chalon" => '10',
                "C'est en ville Dijon" => '11',
                "leprogrès.fr (web)" => '12',
                "lejsl.com (web)" => '13',
                "bienpublic.com (web)" => '14',
            ],
            "expanded" => true,
            "multiple" => true
        ])->add('format', ChoiceType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Format",
            ],
            'choices' => [
                '1/4 page (1500 signes)' => '1',
                '1/2 page (3000 signes)' => '2',
                '1 page (6000 signes)' => '3',
            ],
            "expanded" => true
        ])->add('theme', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Thème du publi",
            ]
        ])->add('observations', TextareaType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Observations",
                'rows' => '6'
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //here for search
    }

    protected function configureListFields(ListMapper $listMapper)
    {

    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/publi.html.twig', array('object' => $object));
        if($object->getDepartement() == 4 or $object->getDepartement() == 1 or $object->getDepartement() == 3 or $object->getDepartement() == 2 or $object->getDepartement() == 5) {
            $emails = ['catherine.bulle@leprogres.fr', 'laurence.cordonier@leprogres.fr'];
        } elseif ($object->getDepartement() == 6) {
            $emails = ['isabelle.dupont@lebienpublic.fr', 'laurence.cordonier@leprogres.fr'];
        }
        $message = \Swift_Message::newInstance();
        $message->setSubject('Nouvelle demande de publication : ' . $object->getRaisonsocialAnnonceur())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/publi.html.twig', array('object' => $object));
        if($object->getDepartement() == 4 or $object->getDepartement() == 1 or $object->getDepartement() == 3 or $object->getDepartement() == 2 or $object->getDepartement() == 5) {
            $emails = ['catherine.bulle@leprogres.fr', 'laurence.cordonier@leprogres.fr'];
        } elseif ($object->getDepartement() == 6) {
            $emails = ['isabelle.dupont@lebienpublic.fr', 'laurence.cordonier@leprogres.fr'];
        }
        $message = \Swift_Message::newInstance();
        $message->setSubject("Modification d'une demande de publication : " . $object->getRaisonsocialAnnonceur())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
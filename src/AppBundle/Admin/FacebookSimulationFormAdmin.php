<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FacebookSimulationFormAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'admin_app_facebook_simulation_form';

    protected $baseRoutePattern = 'admin_app_facebook_simulation_form';

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->andWhere($query->getRootAlias() . '.entryType = :simulation');
        if ($user->hasRole('ROLE_ADMIN')) {
            $query->andWhere($query->getRootAlias() .'.commercial = :commercial');
            $query->setParameter('commercial', $user->getEmail());
        }
        $query->setParameter('simulation', 'simulation');
        $query->orderBy($query->getRootAlias() . '.id', 'DESC');

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('entryType', HiddenType::class, [
            'attr' => [
                'value' => 'simulation'
            ]
        ])->add('commercial', HiddenType::class, [
        ])->add('societe', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Société',
            ]
        ])->add('sitewebClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Site internet du client',
            ]
        ])->add('sexe', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'Hommes' => 'h',
                'Femmes' => 'f',
                'Tout' => 'hf'
            ],
            'expanded' => true
        ])->add('agemin', HiddenType::class, [
            'attr' => [
                'class' => 'agemin'
            ]
        ])->add('agemax', HiddenType::class, [
            'attr' => [
                'class' => 'agemax'
            ]
        ])
            ->add('arrondissement', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    '1er' => '1',
                    '2ème' => '2',
                    '3ème' => '3',
                    '4ème' => '4',
                    '5ème' => '5',
                    '6ème' => '6',
                    '7ème' => '7',
                    '8ème' => '8',
                    '9ème' => '9'
                ],
                'expanded' => true,
                'multiple' => true,
            ])->add('ville1', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('ville2', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('ville3', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('rayon1', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon1'
                ]
            ])->add('rayon2', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon2'
                ]
            ])->add('rayon3', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon3'
                ]
            ])->add('region1', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('region2', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('region3', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('interet1', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '10',
                    'placeholder' => 'Cinéma, musique, parents avec enfants, anniversaires à venir...'
                ]
            ])->add('offre', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    "PACK DATA START" => [
                        "300€ - 5 jours" => '11',
                        "500€ - 5 jours" => '12',
                        "700€ - 5 jours" => '13'
                    ],
                    "PACK DATA PREMIUM" => [
                        "1000€ - 10 jours" => '14',
                        "1500€ - 10 jours" => '15',
                        "2000€ - 10 jours" => '16'
                    ],
                    "Offre Sélection" => '17',
                    'Success Story' => '21',
                    "Dossier partenaire" => '18',
                    "Publi partenaire" => '19',
                    "#MAG" => '20',
                ],
                'expanded' => true,
            ])->add('objectif', ChoiceType::class, [
                'required' => true,
                "attr" => ['class' => 'obj-select'],
                'choices' => [
                    "Augmenter le trafic (Uniquement si le client dispose d'un site web)" => '1',
                    'Augmenter le nombre de fans' => '2',
                    'Notoriété ou événementiel' => '3',
                    'Génération de prospects' => '4',
                    'Vues de vidéos' => '5'
                ],
                'expanded' => true,
            ])->add('observations', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '10',
                    'placeholder' => 'Informations diverses'
                ]
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $datagrid
                ->add('commercial',
                    'doctrine_orm_string',
                    ['show_filter' => true],
                    'choice', ['choices' => $this->getEmailsDomains()]
                );
        }

    }

    protected function getEmailsDomains() {
        $campagnes = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:FacebookForm')->findBy(['entryType' => 'simulation']);
        foreach($campagnes as $campagne) {
            if(strlen($campagne->getCommercial()) != 0) {
                $emails[substr(strtolower(rtrim($campagne->getCommercial())), strpos(strtolower(rtrim($campagne->getCommercial())), '@') + 1)] = substr(strtolower(rtrim($campagne->getCommercial())), strpos(strtolower(rtrim($campagne->getCommercial())), '@') + 1);
            }
            unset($emails['orange.fr']);
            unset($emails['bienpublic.fr']);
            unset($emails['bjp-publicite.com']);
        }

        return $emails;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('ID');
        $listMapper->addIdentifier('societe', 'string', [
            'label' => "Nom Client"
        ]);
        $listMapper->addIdentifier('commercial', 'string', [
            'label' => "Commercial"
        ]);
        $listMapper->addIdentifier('date', 'datetime', [
            'label' => "Date création"
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
                'show' => [],
                'delete' => [],
            ]
        ]);


    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/facebook-simulation.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $emails = array($user->getEmail(), 'bjpfacebook@leprogres.fr');
        $message = \Swift_Message::newInstance();
        $message->setSubject("Demande d'estimation pour : " . $object->getSociete())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/facebook-simulation.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $emails = array($user->getEmail(), 'bjpfacebook@leprogres.fr');
        $message = \Swift_Message::newInstance();
        $message->setSubject("Modfication d'une demande d'estimation pour : " . $object->getSociete())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
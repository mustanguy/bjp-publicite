<?php

namespace AppBundle\Admin;

use Proxies\__CG__\AppBundle\Entity\DocumentsCategory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DocumentCategoryAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_document_category';

    protected $baseRoutePattern = 'admin_app_document_category';


    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('category', TextType::class, [
            'required' => true,
            'attr' => [
                "placeholder" => "Nom de la categorie",
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('category');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('category', null, ['label' => 'Categorie'])->add('_action', null, [
            'actions' => [
                'edit' => [],
                'delete' => [],
            ]
        ]);
    }
}
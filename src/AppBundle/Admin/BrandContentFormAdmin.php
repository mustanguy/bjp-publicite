<?php

namespace AppBundle\Admin;

use AppBundle\Entity\BrandContentForm;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BrandContentFormAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_brand_content_form';

    protected $baseRoutePattern = 'admin_app_brand_content_form';

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        if ($user->hasRole('ROLE_ADMIN')) {
            $query->andWhere($query->getRootAlias() .'.commercial = :commercial');
            $query->setParameter('commercial', $user->getEmail());
        }
        $query->orderBy($query->getRootAlias() .'.id', 'DESC');

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Brand Content";
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('commercial', HiddenType::class, [
        ])->add('numOrdre', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Numéro d'ordre",
            ]
        ])->add('societe', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Société',
            ]
        ])->add('nomClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Nom du client",
            ]
        ])->add('mailClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Mail du client",
            ]
        ])->add('telClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Téléphone du client",
            ]
        ])->add('siteWebClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Site web du client",
            ]
        ])->add('fbPage', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Page facebook du client",
            ]
        ])->add('sexe', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'Hommes' => 'h',
                'Femmes' => 'f',
                'Tout' => 'hf'
            ],
            'expanded' => true
        ])->add('agemin', HiddenType::class, [
            'attr' => [
                'class' => 'agemin'
            ]
        ])->add('agemax', HiddenType::class, [
            'attr' => [
                'class' => 'agemax'
            ]
        ])->add('arrondissement', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    '1er' => '1',
                    '2ème' => '2',
                    '3ème' => '3',
                    '4ème' => '4',
                    '5ème' => '5',
                    '6ème' => '6',
                    '7ème' => '7',
                    '8ème' => '8',
                    '9ème' => '9'
                ],
                'expanded' => true,
                'multiple' => true,
            ])->add('ville1', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('ville2', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('ville3', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('rayon1', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon1'
                ]
            ])->add('rayon2', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon2'
                ]
            ])->add('rayon3', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon3'
                ]
            ])->add('region1', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('region2', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('region3', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('interet1', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '10',
                    'placeholder' => 'Cinéma, musique, parents avec enfants, anniversaires à venir...'
                ]
            ])->add('offre', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    "Dossier partenaire" => '1',
                    "Page partenaire" => '2',

                ],
                'expanded' => true,
            ])->add('url', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => "URL de la page d'atterrissage"
                ]
            ])->add('dateDiffusion', DatePickerType::class, [
                'required' => true,
                'format' => 'dd/MM/yyyy'
            ])
            ->add('observations', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '10',
                    'placeholder' => 'Informations diverses'
                ]
            ]);
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('ID');
        $listMapper->addIdentifier('societe', 'string', [
            'label' => "Nom Client"
        ]);
        $listMapper->addIdentifier('commercial', 'string', [
            'label' => "Commercial"
        ]);
        $listMapper->addIdentifier('date', 'datetime', [
            'label' => "Date création"
        ]);
        $listMapper->addIdentifier('numOrdre', 'string', [
            'label' => "N° Ordre"
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
                'show' => [],
                'delete' => [],
            ]
        ]);
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/brand-content.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $emails = array($user->getEmail(), 'bjpfacebook@leprogres.fr');
        $message = \Swift_Message::newInstance();
        $message->setSubject('Nouveau brand content : ' . $object->getSociete())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/brand-content.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $emails = array($user->getEmail(), 'bjpfacebook@leprogres.fr');
        $message = \Swift_Message::newInstance();
        $message->setSubject("Modification d'un brand content : " . $object->getSociete())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
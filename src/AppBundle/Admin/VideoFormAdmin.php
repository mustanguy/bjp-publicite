<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MapetiteentrepriseCategories;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\Type\FileManagerType;

class VideoFormAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_video_form';

    protected $baseRoutePattern = 'admin_app_video_form';

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        if ($user->hasRole('ROLE_ADMIN')) {
            $query->andWhere($query->getRootAlias() .'.commercial = :commercial');
            $query->setParameter('commercial', $user->getEmail());
        }
        $query->orderBy($query->getRootAlias() . '.id', 'DESC');

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        $actions['delete'];

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $today = new \DateTime();
        $todayplus15 = $today->modify('+15 weekday');

        $formMapper->add('numOrdre',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Numéro d'ordre",
            ]
        ])->add('societe',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Société",
            ]
        ])->add('nomClient',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Nom du client",
            ]
        ])->add('adresseClient',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Adresse du client",
            ]
        ])->add('telClient',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Téléphone du client",
            ]
        ])->add('mailClient',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Email du client",
            ]
        ])->add('sitewebClient',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Site web du client",
            ]
        ])->add('titreVideo',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Titre de la vidéo",
            ]
        ])->add('datelivraison', DateTimePickerType::class, [
            'required' => false,
            'format' => 'dd/MM/yyyy',
            'placeholder' => 'Date de livraison',
            'dp_min_date' => $todayplus15,
        ])->add('observations', TextareaType::class, [
            'required' => false,
            'attr' => ['rows' => '6', 'placeholder' => "Objectifs (soyez le plus précis possible : objectif notoriété, évènement, promotion d'un produit, ...)"]
        ])->add('cible',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Cible",
            ]
        ])->add('lieu',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Lieu de tournage",
            ]
        ])->add('reseauTwitter',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Twitter',
            ]
        ])->add('reseauFacebook',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Facebook',
            ]
        ])->add('reseauViadeo',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Viadeo',
            ]
        ])->add('reseauGoogle',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Google plus',
            ]
        ])->add('reseauLinkedin',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Linkedin',
            ]
        ])->add('reseauInsta',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Instagram',
            ]
        ])->add('reseauYoutube',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Youtube',
            ]
        ])->add('opttech', ChoiceType::class, [
            'required' => false,
            'choices' => [
                '600 € : Motion design (20 secondes)' => '7',
                '600 € : Réalisation d’une vidéo évènementielle ou publicitaire spécifique' => '8',
                '1 000 € : Vues aériennes drone sur site privé et/ou soumis à autorisation' => '9',
                '500 € : Pack 50 photos assuré par un réalisateur' => '10',
                '1 500 € : Pack 50 photos réalisé par un photographe professionnel' => '11'
            ],
            "expanded" => true,
            "multiple" => true
        ])->add('commercial',  HiddenType::class, [
            'required' => false,
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //here for search
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('ID');
        $listMapper->addIdentifier('titreVideo', 'string', [
            'label' => "Titre de la vidéo"
        ]);
        $listMapper->addIdentifier('nomClient', 'string', [
            'label' => "Nom du client"
        ]);
        $listMapper->add('numOrdre', 'string', [
            'label' => "Numéro d'ordre"
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
                'voir' => ['template' => 'video/voir.html.twig'],
                'delete' => [],
            ]
        ]);

    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/video.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $email = [
            'bjpvideo@leprogres.fr',
            $user->getEmail()
        ];

        $message = \Swift_Message::newInstance();
        $message->setSubject('Nouvelle commande de vidéo : ' . $object->getNomClient())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($email)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/video.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $email = [
            'bjpvideo@leprogres.fr',
            $user->getEmail()
        ];

        $message = \Swift_Message::newInstance();
        $message->setSubject("Modification d'une commande de vidéo : " . $object->getNomClient())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($email)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\Type\FileManagerType;

class ProspectiveFormAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_prospective_form';

    protected $baseRoutePattern = 'admin_app_prospective_form';


    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        if ($user->hasRole('ROLE_ADMIN')) {
            $query->andWhere($query->getRootAlias() .'.emailcommercial = :commercial');
            $query->setParameter('commercial', $user->getEmail());
        }
        $query->orderBy($query->getRootAlias() .'.id', 'DESC');

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('media', ChoiceType::class, [
            'required' => true,
            'choices' => [
                'Print' => 'Print',
                'Web' => 'Web',
            ],
            "expanded" => true,
            "multiple" => false
        ])->add('departementcommercial', ChoiceType::class, [
            'required' => true,
            'choices' => [
                '01 - Ain' => '01',
                "21 - Côte-d'or" => '21',
                "39 - Jura" => '39',
                "42 - Loire" => '42',
                "43 - Haute-Loire" => '43',
                "69 - Rhône" => '69',
                "71 - Saône-et-Loire" => '71',
            ],
            "expanded" => false,
            "multiple" => false
        ])->add('nomcommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Nom commercial',
            ]
        ])->add('prenomcommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Prenom commercial',
            ]
        ])->add('emailcommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Email commercial',
            ]
        ])->add('telephonecommercial', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Telephone commercial',
            ]
        ])->add('nomclient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Nom client',
            ]
        ])->add('sitewebclient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Site web du client',
            ]
        ])->add('listeformatsbase', ChoiceType::class, [
            'required' => true,
            'choices' => [
                'Kit prospé (Quart de page, demi page pour le papier & 300x600, 1000x200 pour le web)' => 'Kit prospé',
                'Autre (à préciser dans le briefing)' => 'Autre',
            ],
            "expanded" => true,
            "multiple" => false
        ])->add('listezonediffusionbase', ChoiceType::class, [
            'required' => true,
            'choices' => [
                'Accueil' => 'Accueil',
                'Département' => 'Département',
                'Edition premium' => 'Edition premium',
                'Edition autre' => 'Edition autre',
                'Lifestyle' => 'Lifestyle',
                '# premium' => '# premium',
                '# bis' => '# bis',
                'Pack DATABJP' => 'Pack DATABJP',
            ],
            "expanded" => true,
            "multiple" => false
        ])->add('rubrique', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Rubrique',
            ]
        ])->add('listetypebase', ChoiceType::class, [
            'required' => true,
            'choices' => [
                'Notoriété' => 'Notoriété',
                'Trafic' => 'Trafic',
                'Journées Portes Ouvertes' => 'Journées Portes Ouvertes',
                'Événement' => 'Événement'
            ],
            "expanded" => true,
            "multiple" => true
        ])->add('briefing', TextareaType::class, [
            'required' => false,
            'attr' => ['rows' => '6']
        ])->add('file1', FileManagerType::class,[
            'required' => false,
            'attr' => [
                "placeholder" => "Url du document ou recherche sur l'ordinateur",
            ]
        ])->add('file2', FileManagerType::class,[
            'required' => false,
            'attr' => [
                "placeholder" => "Url du document ou recherche sur l'ordinateur",
            ]
        ])->add('file3', FileManagerType::class,[
            'required' => false,
            'attr' => [
                "placeholder" => "Url du document ou recherche sur l'ordinateur",
            ]
        ])->add('file4', FileManagerType::class,[
            'required' => false,
            'attr' => [
                "placeholder" => "Url du document ou recherche sur l'ordinateur",
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //here for search
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('ID');
        $listMapper->addIdentifier('nomclient', 'string', [
            'label' => "Nom Client"
        ]);
        $listMapper->add('nomcommercial', 'string', [
            'label' => "Commercial"
        ]);
        $listMapper->add('date', 'datetime', [
            'label' => "Date création"
        ]);
        $listMapper->add('sitewebclient', 'string', [
            'label' => "Site du client"
        ]);
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/prospective.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $email = [];
        if ($object->getMedia() == "Web") {
            if ($object->getDepartementcommercial() == "21" ||
                $object->getDepartementcommercial() == '71'  ||
                $object->getDepartementcommercial() == '01') {
                $email[] = 'BRGTRAFICWEB@lebienpublic.fr';
            } elseif ($object->getDepartementcommercial() == '39' ||
                $object->getDepartementcommercial() == '42' ||
                $object->getDepartementcommercial() == '43' ||
                $object->getDepartementcommercial() == '69') {
                $email[] = 'LPRSTUDIOPP1@leprogres.fr';
            }
        } elseif ($object->getMedia() == "Print") {
            if ($object->getDepartementcommercial() == "21") {
                $email[] = 'LBPPREPRESSE@lebienpublic.fr';
            } elseif ($object->getDepartementcommercial() == '71'  || $object->getDepartementcommercial() == '01') {
                $email[] = 'jsl-pressepub@lejsl.fr';
            } elseif ($object->getDepartementcommercial() == '39' ||
                $object->getDepartementcommercial() == '42' ||
                $object->getDepartementcommercial() == '43' ||
                $object->getDepartementcommercial() == '69') {
                $email[] = 'LPRSTUDIOPP1@leprogres.fr';
            }
        }

        $message = \Swift_Message::newInstance();
        $message->setSubject('Nouvelle demande de prospective : ' . $object->getNomclient())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($email)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/prospective.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $email = [];
        if ($object->getMedia() == "Web") {
            if ($object->getDepartementcommercial() == "21" ||
                $object->getDepartementcommercial() == '71'  ||
                $object->getDepartementcommercial() == '01') {
                $email[] = 'BRGTRAFICWEB@lebienpublic.fr';
            } elseif ($object->getDepartementcommercial() == '39' ||
                $object->getDepartementcommercial() == '42' ||
                $object->getDepartementcommercial() == '43' ||
                $object->getDepartementcommercial() == '69') {
                $email[] = 'LPRSTUDIOPP1@leprogres.fr';
            }
        } elseif ($object->getMedia() == "Print") {
            if ($object->getDepartementcommercial() == "21") {
                $email[] = 'LBPPREPRESSE@lebienpublic.fr';
            } elseif ($object->getDepartementcommercial() == '71'  || $object->getDepartementcommercial() == '01') {
                $email[] = 'jsl-pressepub@lejsl.fr';
            } elseif ($object->getDepartementcommercial() == '39' ||
                $object->getDepartementcommercial() == '42' ||
                $object->getDepartementcommercial() == '43' ||
                $object->getDepartementcommercial() == '69') {
                $email[] = 'LPRSTUDIOPP1@leprogres.fr';
            }
        }

        $message = \Swift_Message::newInstance();
        $message->setSubject("Modification d'une demande de prospective : " . $object->getNomclient())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($email)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
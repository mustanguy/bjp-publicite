<?php

namespace AppBundle\Admin;

use AppBundle\AppBundle;
use AppBundle\Entity\MapetiteentrepriseCategories;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\Type\FileManagerType;

class MapetiteentrepriseFormAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_mapetiteentreprise_form';

    protected $baseRoutePattern = 'admin_app_mapetiteentreprise_form';

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->orderBy($query->getRootAlias() .'.id', 'DESC');

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        $actions['delete'];

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('nom',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Titre',
            ]
        ])->add('secteur',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Secteur d'activité",
            ]
        ])->add('idSecteur', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'Rhone /  Lyon' => '17',
                'Saône et Loire / Chalon' => '18',
                'Loire / St Etienne' => '19',
                'Côte d\'or / Dijon' => '20',
                'Jura / Lons-le-Saunier' => '21',
                'Ain / Bourg-en-Bresse' => '22',
                'Haute-Loire / Le Puy' => '23',
                'Haute-Alpes' => '26',
                'Ardèche' => '27',
                'Doubs' => '28',
                'Drôme' => '29',
                'Isère' => '30',
                'Meurthe-et-Moselle' => '31',
                'Meuse' => '32',
                'Moselle' => '33',
                'Bas-Rhin' => '34',
                'Haut-Rhin' => '35',
                'Haute-Saône' => '36',
                'Savoie' => '37',
                'Haute-Savoie' => '38',
                'Vaucluse' => '39',
                'Vosges' => '40',
                'Territoire de Belfort' => '41',
                'Toutes' => 'all',
            ],
            "expanded" => false,
            "multiple" => false
        ])->add('datePub', DatePickerType::class, [
            'required' => false,
            'format' => 'dd/MM/yyyy'
        ])->add('chapo', TextareaType::class, [
            'required' => false,
            'attr' => ['rows' => '6']
        ])->add('thumb',  HiddenType::class,[
            'required' => false,
            'attr' => [
                "class" => "thumb",
            ]
        ])->add('bonplan',HiddenType::class ,[
            'required' => false,
            'attr' => [
                'class' => 'bonplan'
            ]
        ])->add('description',TextareaType::class ,[
            'required' => false,
            'attr' => ['rows' => '6']
        ])->add('descriptionLongue',TextareaType::class ,[
            'required' => false,
            'attr' => ['rows' => '6']
        ])->add('email',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Email du client',
            ]
        ])->add('nomEntreprise',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => "Nom de l'entreprise",
            ]
        ])->add('adresse',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Adresse',
            ]
        ])->add('cp',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Code postal',
            ]
        ])->add('ville',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Ville',
            ]
        ])->add('tel1',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Tel 1',
            ]
        ])->add('tel2',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Tel 2',
            ]
        ])->add('twitter',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Twitter',
            ]
        ])->add('facebook',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Facebook',
            ]
        ])->add('viadeo',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Viadeo',
            ]
        ])->add('googleplus',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Google plus',
            ]
        ])->add('linkedin',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Linkedin',
            ]
        ])->add('instagram',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Instagram',
            ]
        ])->add('youtube',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Youtube',
            ]
        ])->add('site',  TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url Site Web',
            ]
        ])->add('lat',HiddenType::class ,[
            'required' => false,
            'attr' => [
                'class' => 'lat'
            ]
        ])->add('lng',HiddenType::class ,[
            'required' => false,
            'attr' => [
                'class' => 'lng'
            ]
        ])->add('embed1',TextareaType::class ,[
            'required' => false,
            'attr' => ['rows' => '6']
        ])->add('status', HiddenType::class, [
            'attr' => [
                'value' => '0'
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //here for search
    }



    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('ID');
        $listMapper->addIdentifier('nom', 'string', [
            'label' => "Nom"
        ]);
        $listMapper->add('status', 'choice', [
            'label' => "Status",
            'choices' => [
                0 => 'Non publié',
                1 => 'Publié',
            ],
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'publier' => ['template' => 'mapetiteentreprise/publier.html.twig'],
                'edit' => [],
                'delete' => [],
            ]
        ]);

    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/mapetitentreprise.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $email = [
            'bjpvideo@leprogres.fr',
            $user->getEmail()
        ];

        $message = \Swift_Message::newInstance();
        $message->setSubject('Nouvelle publication de vidéo : ' . $object->getNomEntreprise())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($email)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/mapetitentreprise.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $email = [
            'bjpvideo@leprogres.fr',
            $user->getEmail()
        ];

        $message = \Swift_Message::newInstance();
        $message->setSubject("Modification d'une vidéo : " . $object->getNomEntreprise())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($email)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
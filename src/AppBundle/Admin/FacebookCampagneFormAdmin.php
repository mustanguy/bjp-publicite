<?php
// src/Admin/CategoryAdmin.php
namespace AppBundle\Admin;

use AppBundle\Entity\FacebookForm;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FacebookCampagneFormAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_facebook_campagne_form';

    protected $baseRoutePattern = 'admin_app_facebook_campagne_form';

    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->andWhere($query->getRootAlias() .'.entryType = :campagne');
        if ($user->hasRole('ROLE_ADMIN')) {
            $query->andWhere($query->getRootAlias() .'.commercial = :commercial');
            $query->setParameter('commercial', $user->getEmail());
        }
        $query->setParameter('campagne', 'campagne');
        $query->orderBy($query->getRootAlias() .'.id', 'DESC');

        return $query;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    public function configure()
    {
        parent::configure();
        $this->classnameLabel = "Facebook Ads campagne";
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('entryType', HiddenType::class, [
            'attr' => [
                'value' => 'campagne'
            ]
        ])->add('commercial', HiddenType::class, [
        ])->add('numOrdre', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Numéro d'ordre",
            ]
        ])->add('societe', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Société',
            ]
        ])->add('nomClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Nom du client",
            ]
        ])->add('mailClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Email du client",
            ]
        ])->add('telClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => "Téléphone client",
            ]
        ])->add('sitewebClient', TextType::class, [
            'required' => true,
            'attr' => [
                'placeholder' => 'Site internet du client',
            ]
        ])->add('fbPage', ChoiceType::class, [
            'required' => true,
            'choices' => [
                'Créer une page pour le client' => '1',
                'Mise à jour de la page du client' => '3',
                'Le client donne la délégation d\'administrateur de sa page' => '0',
                'Utiliser la page Facebook du titre (Offre Sélection et Success Story)' => '4',
            ],
            'expanded' => true,
        ])->add('pageClient', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url de la page du client'
            ]
        ])->add('pageclientpourmaj', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url de la page du client'
            ]
        ])->add('clientfblogin', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Login page Facebook'
            ]
        ])->add('clientfbpwd', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Mot de passe page Facebook'
            ]
        ])->add('activiteClient', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Activité du client'
            ]
        ])->add('horairesClient', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Horaires client'
            ]
        ])->add('typeBtn', ChoiceType::class, [
            'required' => false,
            'choices' => [
                "Entrer en contact" => [
                    'Appeler' => '1',
                    'Envoyer un message sur facebook Messenger' => '2',
                    'Envoyer un message par mail' => '3',
                    'Rediriger vers le formulaire de contact du site web' => '4',
                    'Rediriger vers le formulaire d\'inscription du site web' => '5',
                ],
                "Service de réservation" => [
                    'Rediriger vers un site web pour réserver' => '6',
                    'Passer une commande via Facebook' => '7',
                ],
                "En savoir plus" => [
                    'Rediriger vers une vidéo sur la page Facebook ou sur le site' => '8',
                    'Rediriger vers une page avec plus d\'informations' => '9',
                ],
                "Acheter / Faire un don" => [
                    'Rediriger vers un achat sur le site web ou sur la page Facebook' => '10',
                    'Rediriger vers une offre spéciale de la page Facebook' => '11',
                ],
                "Télécharger l'app ou le jeu" => [
                    'Rediriger vers l\'application' => '12',
                    'Rediriger vers une page afin de pouvoir télécharger et jouer au jeu' => '13',
                ],
            ],
            'placeholder' => 'Type de bouton',
            'expanded' => false,
        ])->add('urlRedirect', TextType::class, [
            'required' => false,
            'attr' => [
                'placeholder' => 'Url de redirection'
            ]
        ])->add('sexe', ChoiceType::class, [
            'required' => false,
            'choices' => [
                'Hommes' => 'h',
                'Femmes' => 'f',
                'Tout' => 'hf'
            ],
            'expanded' => true
        ])->add('agemin', HiddenType::class, [
            'attr' => [
                'class' => 'agemin'
            ]
        ])->add('agemax', HiddenType::class, [
            'attr' => [
                'class' => 'agemax'
            ]
        ])
            ->add('arrondissement', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    '1er' => '1',
                    '2ème' => '2',
                    '3ème' => '3',
                    '4ème' => '4',
                    '5ème' => '5',
                    '6ème' => '6',
                    '7ème' => '7',
                    '8ème' => '8',
                    '9ème' => '9'
                ],
                'expanded' => true,
                'multiple' => true,
            ])->add('ville1', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('ville2', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('ville3', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Ville'
                ]
            ])->add('rayon1', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon1'
                ]
            ])->add('rayon2', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon2'
                ]
            ])->add('rayon3', HiddenType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'rayon3'
                ]
            ])->add('region1', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('region2', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('region3', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Région'
                ]
            ])->add('interet1', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '10',
                    'placeholder' => 'Cinéma, musique, parents avec enfants, anniversaires à venir...'
                ]
            ])->add('offre', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    "PACK DATA START" => [
                        "300€ - 5 jours" => '11',
                        "500€ - 5 jours" => '12',
                        "700€ - 5 jours" => '13'
                    ],
                    "PACK DATA PREMIUM" => [
                        "1000€ - 10 jours" => '14',
                        "1500€ - 10 jours" => '15',
                        "2000€ - 10 jours" => '16'
                    ],
                    "Offre Sélection" => '17',
                    'Success Story' => '21',
                    "Dossier partenaire" => '18',
                    "Publi partenaire" => '19',
                    "#MAG" => '20',
                ],
                'expanded' => true,
            ])->add('objectif', ChoiceType::class, [
                'required' => true,
                'choice_attr' => function($category, $key, $value) {
                    return ['class' => 'objectif-radio'];
                },
                'choices' => [
                    'Augmenter le trafic (uniquement si le client dispose d’un site web !)' => '1',
                    'Augmenter le nombre de fans' => '2',
                    'Notoriété ou événementiel' => '3',
                    'Génération de prospects' => '4',
                    'Vues de vidéos' => '5'
                ],
                'expanded' => true,
            ])->add('datecampagne', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'date-campagne',
                    'placeholder' => 'Date de la campagne'
                ]
            ])->add('type', ChoiceType::class, [
                'required' => false,
                'choices' => [
                    'Carrousel' => '1',
                    'Image unique' => '2',
                    'Vidéo unique' => '3',
                    'Diaporama' => '4',
                ],
                'choice_attr' => function($category, $key, $value) {
                    return ['class' => 'type-'.$value];
                },
                'expanded' => true,
            ])->add('titreCampagne', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'titre-campagne',
                    'placeholder' => 'Titre de la campagne'
                ]
            ])->add('url', TextType::class, [
                'required' => false,
                'attr' => [
                    'class' => 'url-dest',
                    'placeholder' => 'http://'
                ]
            ])->add('observations', TextareaType::class, [
                'required' => false,
                'attr' => [
                    'rows' => '10',
                    'placeholder' => 'Informations diverses'
                ]
            ]);
    }


    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            $datagrid
                ->add('commercial',
                    'doctrine_orm_string',
                    ['show_filter' => true],
                    'choice', ['choices' => $this->getEmailsDomains()]
                );
        }

    }

    protected function getEmailsDomains() {
        $campagnes = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->getRepository('AppBundle:FacebookForm')->findBy(['entryType' => 'campagne']);
        foreach($campagnes as $campagne) {
            if(strlen(($campagne->getCommercial())) != 0) {
                $emails[substr(strtolower(rtrim($campagne->getCommercial())), strpos(strtolower(rtrim($campagne->getCommercial())), '@') + 1)] = substr(strtolower(rtrim($campagne->getCommercial())), strpos(strtolower(rtrim($campagne->getCommercial())), '@') + 1);
            }
            unset($emails['orange.fr']);
            unset($emails['bienpublic.fr']);
            unset($emails['bjp-publicite.com']);
        }

        return $emails;
    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('ID');
        $listMapper->addIdentifier('societe', 'string', [
            'label' => "Nom Client"
        ]);
        $listMapper->addIdentifier('commercial', 'string', [
            'label' => "Commercial"
        ]);
        $listMapper->addIdentifier('date', 'datetime', [
            'label' => "Date création"
        ]);
        $listMapper->addIdentifier('numOrdre', 'string', [
            'label' => "N° Ordre"
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
                'show' => [],
                'delete' => [],
            ]
        ]);
    }

    public function postPersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/facebook-campagne.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $emails = array($user->getEmail(), 'bjpfacebook@leprogres.fr');
        $message = \Swift_Message::newInstance();
        $message->setSubject('Nouvelle campagne client : ' . $object->getSociete())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }

    public function postUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();

        $body = $container->get('templating')->render('email/facebook-campagne.html.twig', array('object' => $object));
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $emails = array($user->getEmail(), 'bjpfacebook@leprogres.fr');
        $message = \Swift_Message::newInstance();
        $message->setSubject("Modification d'une campagne client : " . $object->getSociete())
            ->setFrom('admin@bjp-publicite.com')
            ->setTo($emails)
            ->setBody($body, 'text/html');

        $container->get('mailer')->send($message);
    }
}
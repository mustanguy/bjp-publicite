<?php

namespace AppBundle\Admin;

use AppBundle\Form\Type\FileManagerType;
use Proxies\__CG__\AppBundle\Entity\DocumentsCategory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DocumentAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'admin_app_document';

    protected $baseRoutePattern = 'admin_app_document';

    protected $searchResultActions = ['edit', 'show'];

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class, [
            'required' => true,
            'attr' => [
                "placeholder" => "Nom du document",
            ]
        ])->add('url', FileManagerType::class,[
            'required' => false,
            'attr' => [
                "placeholder" => "Url du document ou recherche sur l'ordinateur",
            ]
        ])->add('category', EntityType::class, [
            'required' => true,
            'class' => DocumentsCategory::class,
            'choice_label' => 'category',
            'expanded' => false,
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name')->add('url')->add('category.category', null, ['label' => 'Categorie'])->add('_action', null, [
            'actions' => [
                'edit' => [],
                'delete' => [],
            ]
        ]);
    }
}